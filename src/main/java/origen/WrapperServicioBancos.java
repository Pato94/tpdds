package origen;

import busqueda.Busqueda;
import busqueda.BusquedaBancos;
import com.fasterxml.jackson.databind.type.CollectionType;
import external.BancoResponse;
import external.ServicioRestBancos;
import poi.Banco;
import utils.ResponseMapper;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class WrapperServicioBancos extends OrigenDePOIs {
    private final ServicioRestBancos servicio;
    private final BancosResponseMapper responseMapper;
    private final static List<Class<? extends Busqueda>> busquedasPermitidas =
            Collections.singletonList(BusquedaBancos.class);

    public WrapperServicioBancos(ServicioRestBancos servicio) {
        this.servicio = servicio;
        this.responseMapper = new BancosResponseMapper();
    }

    @Override
    public List<Banco> resolverBusquedaBancos(BusquedaBancos busqueda) {
        return responseMapper
                .mapResponse(servicio.buscarBancos(busqueda.getNombreBanco(), busqueda.getNombreServicio()))
                .stream().map(bancoResponse -> bancoResponse.toBanco())
                .collect(Collectors.toList());
    }

    @Override
    public List<Class<? extends Busqueda>> busquedasPermitidas() {
        return busquedasPermitidas;
    }

    private class BancosResponseMapper extends ResponseMapper<List<BancoResponse>> {
        @Override
        protected CollectionType createTypeFactory() {
            return getMapper().getTypeFactory().constructCollectionType(List.class, BancoResponse.class);
        }
    }
}
