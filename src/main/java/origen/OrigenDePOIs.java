package origen;

import busqueda.Busqueda;
import busqueda.BusquedaBancos;
import busqueda.BusquedaCGPs;
import busqueda.BusquedaEnMapa;
import poi.POI;

import java.util.List;

public abstract class OrigenDePOIs {
    public abstract List<Class<? extends Busqueda>> busquedasPermitidas();

    public List<? extends POI> resolverBusquedaMapa(BusquedaEnMapa busqueda) {
        throw new UnsupportedOperationException("Busqueda en mapa no implementada");
    }

    ;

    public List<? extends POI> resolverBusquedaCGP(BusquedaCGPs busqueda) {
        throw new UnsupportedOperationException("Busqueda en cgps no implementada");
    }

    ;

    public List<? extends POI> resolverBusquedaBancos(BusquedaBancos busqueda) {
        throw new UnsupportedOperationException("Busqueda en bancos no implementada");
    }

    ;
}
