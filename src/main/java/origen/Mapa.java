package origen;

import busqueda.Busqueda;
import busqueda.BusquedaEnMapa;
import poi.POI;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Mapa extends OrigenDePOIs {

    private List<POI> pois;
    private static final List<Class<? extends Busqueda>> busquedasPermitidas =
            Collections.singletonList(BusquedaEnMapa.class);

    public Mapa(List<POI> pois) {
        this.pois = pois;
    }

    public List<POI> buscarPois(String busqueda) {
        return pois.stream()
                .filter(poi -> poi.coincideCon(busqueda))
                .collect(Collectors.toList());
    }

    public List<POI> todosLosPOIs() {
        return this.pois;
    }

    public void agregarPOI(POI poi) {
        this.pois.add(poi);
    }

    public void removerPOI(POI poi) {
        this.pois.remove(poi);
    }

    public void modificarPOI(POI poiViejo, POI poiNuevo) {
        removerPOI(poiViejo);
        agregarPOI(poiNuevo);
    }

    @Override
    public List<POI> resolverBusquedaMapa(BusquedaEnMapa busqueda) {
        return this.buscarPois(busqueda.getBusqueda());
    }

    @Override
    public List<Class<? extends Busqueda>> busquedasPermitidas() {
        return busquedasPermitidas;
    }
}