package origen;

import busqueda.Busqueda;
import busqueda.BusquedaCGPs;
import external.ComponenteCGPs;
import poi.CGP;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class WrapperComponenteCGPs extends OrigenDePOIs {

    private ComponenteCGPs componenteExterno;
    private static final List<Class<? extends Busqueda>> busquedasPermitidas =
            Collections.singletonList(BusquedaCGPs.class);

    public WrapperComponenteCGPs(ComponenteCGPs componenteExterno) {
        this.componenteExterno = componenteExterno;
    }

    @Override
    public List<CGP> resolverBusquedaCGP(BusquedaCGPs busqueda) {
        return this.componenteExterno
                .buscarCGPs(busqueda.getBusqueda())
                .stream().map(CGP::fromCentroDTO)
                .collect(Collectors.toList());
    }

    @Override
    public List<Class<? extends Busqueda>> busquedasPermitidas() {
        return busquedasPermitidas;
    }
}
