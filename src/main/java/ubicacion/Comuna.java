package ubicacion;

import org.uqbar.geodds.Polygon;

import java.util.List;
import java.util.stream.Collectors;

public class Comuna {

    private String name;
    private Polygon polygon;

    public Comuna(String name, List<Ubicacion> vertices) {
        this.name = name;
        this.polygon = new Polygon(vertices.stream().map(ubicacion -> ubicacion.getPoint()).collect(Collectors.toList()));
    }

    public boolean contieneA(Ubicacion ubicacion) {
        return this.polygon.isInside(ubicacion.getPoint());
    }

    public String getName() {
        return name;
    }
}