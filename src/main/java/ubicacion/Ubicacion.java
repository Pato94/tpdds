package ubicacion;

import org.uqbar.geodds.Point;

public class Ubicacion {
    public final Point point;

    public Ubicacion(double latitude, double longitude) {
        this(new Point(latitude, longitude));
    }

    public Ubicacion(Point point) {
        this.point = point;
    }

    public double distanciaA(Ubicacion ubicacion) {
        //Nuestro framework nos da la distancia en kilometros
        //La multiplico por mil para obtenerla en metros
        return this.point.distance(ubicacion.getPoint()) * 1000;
    }

    public Point getPoint() {
        return point;
    }
}
