package usuario;

import accion.AccionSistema;
import accion.AccionesDesactivadasException;
import ubicacion.Comuna;

import java.util.LinkedList;
import java.util.List;

public class Usuario {

    private final String nombre;
    private final String email;
    private final Comuna comuna;
    private final List<Class<? extends AccionSistema>> accionesPermitidas;

    public Usuario(String nombre, String email, Comuna comuna) {
        this.nombre = nombre;
        this.email = email;
        this.comuna = comuna;
        this.accionesPermitidas = new LinkedList<>();
    }

    public String getEmail() {
        return email;
    }

    public String getNombre() {
        return nombre;
    }

    public Comuna getComuna() {
        return comuna;
    }

    public List<Class<? extends AccionSistema>> getAccionesPermitidas() {
        return accionesPermitidas;
    }

    public void habilitarAcciones(List<Class<? extends AccionSistema>> acciones) {
        acciones.forEach(accion -> habilitarAccion(accion));
    }

    public void deshabilitarAcciones(List<Class<? extends AccionSistema>> acciones) {
        acciones.forEach(accion -> deshabilitarAccion(accion));
    }

    public void habilitarAccion(Class<? extends AccionSistema> accion) {
        if (!accionesPermitidas.contains(accion)) {
            accionesPermitidas.add(accion);
        }
    }

    public void deshabilitarAccion(Class<? extends AccionSistema> accion) {
        accionesPermitidas.remove(accion);
    }

    public void realizarAccion(AccionSistema accion) {
        if (accionesPermitidas.contains(accion.getClass())) {
            accion.realizar(this);
        } else {
            throw new AccionesDesactivadasException();
        }
    }
}
