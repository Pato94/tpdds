package usuario;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Pato on 9/16/16.
 */
public class RepositorioUsuarios {

    private final List<Usuario> usuarios;

    public RepositorioUsuarios() {
        this(new LinkedList<>());
    }

    public RepositorioUsuarios(List<Usuario> usuarios) {
        this.usuarios = usuarios;
    }

    public List<Usuario> getUsuarios() {
        return this.usuarios;
    }
}
