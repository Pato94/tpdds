package usuario;

import accion.AccionSistema;
import accion.AccionesDesactivadasException;
import ubicacion.Comuna;

public class Terminal extends Usuario {

    private static boolean accionesHabilitadas = true;

    public static void setAccionesHabilitadas(boolean habilitadas) {
        Terminal.accionesHabilitadas = habilitadas;
    }

    public Terminal(String nombre, String email, Comuna comuna) {
        super(nombre, email, comuna);
    }

    public void realizarAccion(AccionSistema accion) {
        if (Terminal.accionesHabilitadas) {
            super.realizarAccion(accion);
        } else {
            throw new AccionesDesactivadasException();
        }
    }
}
