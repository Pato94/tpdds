package consulta;

import java.time.LocalDateTime;

public class ConsultaDisponibilidad {

    private final String nombreServicio;
    private final LocalDateTime momento;

    public ConsultaDisponibilidad(LocalDateTime momento) {
        this(momento, null);
    }

    public ConsultaDisponibilidad(LocalDateTime momento, String nombreServicio) {
        this.momento = momento;
        this.nombreServicio = nombreServicio;
    }

    public LocalDateTime getMomento() {
        return this.momento;
    }

    public String getNombreServicio() {
        return this.nombreServicio;
    }

    public boolean servicioIngresado() {
        return this.nombreServicio != null;
    }
}
