package consulta;

import ubicacion.Comuna;
import ubicacion.Ubicacion;

public class ConsultaCercania {

    private Ubicacion ubicacion;
    private Comuna comuna;

    public ConsultaCercania(Ubicacion ubicacion, Comuna comuna) {
        this.ubicacion = ubicacion;
        this.comuna = comuna;
    }

    public ConsultaCercania(Ubicacion ubicacion) {
        this(ubicacion, null);
    }

    public Ubicacion getUbicacion() {
        return ubicacion;
    }

    public Comuna getComuna() {
        return comuna;
    }
}
