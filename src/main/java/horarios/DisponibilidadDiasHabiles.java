package horarios;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.List;

public class DisponibilidadDiasHabiles implements DisponibilidadHoraria {

    private RangoHorario rangoHorario;
    private static List<DayOfWeek> diasHabiles = Arrays.asList(DayOfWeek.MONDAY, DayOfWeek.TUESDAY, DayOfWeek.WEDNESDAY,
            DayOfWeek.THURSDAY, DayOfWeek.FRIDAY);

    public DisponibilidadDiasHabiles(LocalTime horarioApertura, LocalTime horarioCierre) {
        this(new RangoHorario(horarioApertura, horarioCierre));
    }

    public DisponibilidadDiasHabiles(RangoHorario rangoHorario) {
        this.rangoHorario = rangoHorario;
    }

    @Override
    public boolean disponible(LocalDateTime momento) {
        return diasHabiles.contains(momento.getDayOfWeek()) && this.rangoHorario.comprende(momento.toLocalTime());
    }
}
