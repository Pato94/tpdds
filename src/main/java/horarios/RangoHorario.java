package horarios;

import java.time.LocalTime;

public class RangoHorario {

    private LocalTime comienzo;
    private LocalTime finalizacion;

    public RangoHorario(LocalTime comienzo, LocalTime finalizacion) {
        this.comienzo = comienzo;
        this.finalizacion = finalizacion;
    }

    public boolean comprende(LocalTime momento) {
        return momento.isAfter(this.comienzo) && momento.isBefore(this.finalizacion);
    }
}
