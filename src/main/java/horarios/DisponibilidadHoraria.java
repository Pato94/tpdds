package horarios;

import java.time.LocalDateTime;

public interface DisponibilidadHoraria {
    boolean disponible(LocalDateTime momento);
}
