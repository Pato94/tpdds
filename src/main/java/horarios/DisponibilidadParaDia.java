package horarios;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class DisponibilidadParaDia implements DisponibilidadHoraria {

    private DayOfWeek diaDeLaSemana;
    private RangoHorario rangoHorario;

    public DisponibilidadParaDia(DayOfWeek diaDeLaSemana, LocalTime horarioApertura, LocalTime horarioCierre) {
        this(diaDeLaSemana, new RangoHorario(horarioApertura, horarioCierre));
    }

    public DisponibilidadParaDia(DayOfWeek diaDeLaSemana, RangoHorario rangoHorario) {
        this.diaDeLaSemana = diaDeLaSemana;
        this.rangoHorario = rangoHorario;
    }

    public boolean disponible(DayOfWeek diaDeLaSemana, LocalTime momento) {
        return this.diaDeLaSemana == diaDeLaSemana && rangoHorario.comprende(momento);
    }

    @Override
    public boolean disponible(LocalDateTime momento) {
        return disponible(momento.getDayOfWeek(), momento.toLocalTime());
    }
}
