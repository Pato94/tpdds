package horarios;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

public class DisponibilidadCompuesta implements DisponibilidadHoraria {

    private List<DisponibilidadHoraria> disponibilidades;

    public DisponibilidadCompuesta() {
        this(new LinkedList<DisponibilidadHoraria>());
    }

    public DisponibilidadCompuesta(List<DisponibilidadHoraria> disponibilidades) {
        this.disponibilidades = disponibilidades;
    }

    public void agregarDisponibilidad(DisponibilidadHoraria disponibilidad) {
        disponibilidades.add(disponibilidad);
    }

    @Override
    public boolean disponible(LocalDateTime momento) {
        return disponibilidades.stream().anyMatch(disponibilidad -> disponibilidad.disponible(momento));
    }
}
