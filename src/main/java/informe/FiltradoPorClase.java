package informe;

import busqueda.Busqueda;

/**
 * Creo que ademas de la clase especificada el filtro deja pasar
 * Las subclases de la clase ingresada, no creo que sea un problema
 */
public class FiltradoPorClase extends InformeBusquedaDecorado {

    private Class<? extends Busqueda> klazz;

    public FiltradoPorClase(InformeBusqueda informeADecorar, Class<? extends Busqueda> klazz) {
        super(informeADecorar);
        this.klazz = klazz;
    }

    @Override
    public boolean filter(Busqueda busqueda) {
        return klazz.isInstance(busqueda);
    }
}
