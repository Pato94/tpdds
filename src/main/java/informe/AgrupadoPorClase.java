package informe;

import busqueda.Busqueda;

public class AgrupadoPorClase extends InformeBusquedaDecorado {

    public AgrupadoPorClase(InformeBusquedaBase informeADecorar) {
        super(informeADecorar);
    }

    @Override
    public String nombreGrupo(Busqueda busqueda) {
        return busqueda.getClass().getSimpleName();
    }
}
