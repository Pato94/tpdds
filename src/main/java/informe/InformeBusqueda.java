package informe;

import busqueda.Busqueda;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public abstract class InformeBusqueda {

    /**
     * La paja de no hacer los fors a mano es que no tengo ni idea de el orden de salida
     * No me voy a meter en ese codigo, en mi computadora parece deterministico
     */
    public List<ItemInformeBusqueda> generarInforme(List<Busqueda> datos) {
        Map<String, List<Busqueda>> agrupadas = datos.stream()
                .filter(busqueda -> filter(busqueda))
                .collect(Collectors.groupingBy(busqueda -> nombreGrupo(busqueda).trim()));

        return agrupadas.entrySet().stream()
                .map(entry -> new ItemInformeBusqueda(entry.getKey(), entry.getValue().size()))
                .collect(Collectors.toList());
    }

    public abstract boolean filter(Busqueda busqueda);

    public abstract String nombreGrupo(Busqueda busqueda);
}
