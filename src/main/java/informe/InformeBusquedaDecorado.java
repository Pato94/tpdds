package informe;

import busqueda.Busqueda;

public abstract class InformeBusquedaDecorado extends InformeBusqueda {
    private informe.InformeBusqueda informeADecorar;

    public InformeBusquedaDecorado(InformeBusqueda informeADecorar) {
        this.informeADecorar = informeADecorar;
    }

    @Override
    public boolean filter(Busqueda busqueda) {
        return informeADecorar.filter(busqueda);
    }

    @Override
    public String nombreGrupo(Busqueda busqueda) {
        return informeADecorar.nombreGrupo(busqueda);
    }
}