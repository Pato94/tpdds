package informe;

import busqueda.Busqueda;

public class FiltradoPorNombreUsuario extends InformeBusquedaDecorado {

    private String nombre;

    public FiltradoPorNombreUsuario(InformeBusqueda informeADecorar, String nombre) {
        super(informeADecorar);
        this.nombre = nombre;
    }

    @Override
    public boolean filter(Busqueda busqueda) {
        return busqueda.getUsuario().getNombre().equalsIgnoreCase(nombre);
    }
}
