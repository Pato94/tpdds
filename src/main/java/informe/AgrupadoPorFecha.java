package informe;

import busqueda.Busqueda;

import java.time.format.DateTimeFormatter;

public class AgrupadoPorFecha extends InformeBusquedaDecorado {

    public DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    public AgrupadoPorFecha(InformeBusquedaBase informeADecorar) {
        super(informeADecorar);
    }

    @Override
    public String nombreGrupo(Busqueda busqueda) {
        return busqueda.getComienzo().format(formatter) + " " + super.nombreGrupo(busqueda);
    }
}