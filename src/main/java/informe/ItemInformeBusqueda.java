package informe;

public class ItemInformeBusqueda {
    private final String grupo;
    private final int cantidad;

    public ItemInformeBusqueda(String grupo, int cantidad) {
        this.grupo = grupo;
        this.cantidad = cantidad;
    }

    public String getGrupo() {
        return grupo;
    }

    public int getCantidad() {
        return cantidad;
    }
}
