package informe;

import busqueda.Busqueda;

public class AgrupadoPorNombreUsuario extends InformeBusquedaDecorado {

    public AgrupadoPorNombreUsuario(InformeBusqueda informeADecorar) {
        super(informeADecorar);
    }

    @Override
    public String nombreGrupo(Busqueda busqueda) {
        return busqueda.getUsuario().getNombre();
    }
}
