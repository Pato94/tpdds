package proceso;

import proceso.bombero.Bombero;

import java.time.LocalDateTime;

public abstract class Trabajo {

    private final Despertador despertador;
    private final Bombero bombero;

//	public Trabajo() {
//		this(new DespertadorEstandar(), null);
//	}

//	public Trabajo(Despertador despertador) {
//		this(despertador, null);
//	}

    public Trabajo(Despertador despertador, Bombero bombero) {
        this.bombero = bombero;
        this.despertador = new DespertadorEstandar();
    }

    public void ejecutarEn(LocalDateTime momento, Jefe interesado) {
        despertador.programarPara(momento, () -> interesado.ponerseEnFila(this));
    }

    public void ejecutarAhora(Jefe interesado) {
        interesado.ponerseEnFila(this);
    }

    public void ejecutar(Jefe interesado) {
        new Thread(() -> {
            try {
                ejecutarProceso();
                interesado.termine(ResultadoEjecucion.exito(getElementosAfectados()));
            } catch (Exception e) {
                bombero.manejarSituacion(e, this, interesado);
            }
        }).start();
    }

    public abstract int getElementosAfectados();

    protected abstract void ejecutarProceso();

    public interface Jefe {
        void ponerseEnFila(Trabajo trabajo);

        void termine(ResultadoEjecucion resultado);
    }
}
