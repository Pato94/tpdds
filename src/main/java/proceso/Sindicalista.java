package proceso;

import java.util.LinkedList;
import java.util.Queue;

public class Sindicalista implements Trabajo.Jefe {

    private final LinkedList<ResultadoEjecucion> resultados;
    private Queue<Trabajo> colaTrabajos;
    private boolean ocupado = false;

    public Sindicalista() {
        colaTrabajos = new LinkedList<>();
        resultados = new LinkedList<>();
    }

    private void pullJobIfReady() {
        if (!ocupado) {
            pullJob();
        }
    }

    private void pullJob() {
        if (!colaTrabajos.isEmpty()) {
            ocupado = true;
            Trabajo job = colaTrabajos.poll();
            job.ejecutar(this);
        }
    }

    @Override
    public void ponerseEnFila(Trabajo trabajo) {
        colaTrabajos.add(trabajo);
        pullJobIfReady();
    }

    @Override
    public void termine(ResultadoEjecucion resultado) {
        ocupado = false;
        pullJob();
        resultados.add(resultado);
    }
}
