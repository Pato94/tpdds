package proceso.accionesusuarios;

import usuario.Usuario;

import java.util.List;

/**
 * Created by Pato on 9/16/16.
 */
public class ListaDeterminada implements ProveedorUsuarios {

    private final List<Usuario> usuarios;

    public ListaDeterminada(List<Usuario> usuarios) {
        this.usuarios = usuarios;
    }

    @Override
    public List<Usuario> getUsuarios() {
        return usuarios;
    }
}
