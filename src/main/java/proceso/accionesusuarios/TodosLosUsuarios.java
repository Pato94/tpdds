package proceso.accionesusuarios;

import usuario.RepositorioUsuarios;
import usuario.Usuario;

import java.util.List;

/**
 * Created by Pato on 9/16/16.
 * Obviamente el RepositorioUsuarios podria directamente implementar la interfaz
 * pero me parece que aporta algo de semantica
 */
public class TodosLosUsuarios implements ProveedorUsuarios {

    private final RepositorioUsuarios repositorioUsuarios;

    public TodosLosUsuarios(RepositorioUsuarios repositorioUsuarios) {
        this.repositorioUsuarios = repositorioUsuarios;
    }

    @Override
    public List<Usuario> getUsuarios() {
        return repositorioUsuarios.getUsuarios();
    }
}
