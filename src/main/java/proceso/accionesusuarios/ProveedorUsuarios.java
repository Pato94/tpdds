package proceso.accionesusuarios;

import usuario.Usuario;

import java.util.List;

/**
 * Created by Pato on 9/16/16.
 */
public interface ProveedorUsuarios {
    List<Usuario> getUsuarios();
}
