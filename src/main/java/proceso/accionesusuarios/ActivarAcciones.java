package proceso.accionesusuarios;

import accion.AccionSistema;
import proceso.Despertador;
import proceso.Trabajo;
import proceso.bombero.Bombero;
import usuario.Usuario;

import java.util.List;

/**
 * Created by Pato on 9/16/16.
 */
public class ActivarAcciones extends Trabajo {

    private final ProveedorUsuarios proveedor;
    private final List<Class<? extends AccionSistema>> acciones;
    private int elementosAfectados;

    public ActivarAcciones(ProveedorUsuarios proveedor, List<Class<? extends AccionSistema>> acciones, Despertador despertador, Bombero bombero) {
        super(despertador, bombero);
        this.proveedor = proveedor;
        this.acciones = acciones;
        this.elementosAfectados = 0;
    }

    @Override
    public int getElementosAfectados() {
        return elementosAfectados;
    }

    @Override
    protected void ejecutarProceso() {
        proveedor.getUsuarios()
            .forEach(usuario -> {
                usuario.habilitarAcciones(acciones);
                elementosAfectados++;
            });
    }
}
