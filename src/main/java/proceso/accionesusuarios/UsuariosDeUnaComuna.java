package proceso.accionesusuarios;

import ubicacion.Comuna;
import usuario.RepositorioUsuarios;
import usuario.Usuario;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Pato on 9/16/16.
 */
public class UsuariosDeUnaComuna implements ProveedorUsuarios {

    private final RepositorioUsuarios repositorioUsuarios;
    private final Comuna comuna;

    public UsuariosDeUnaComuna(RepositorioUsuarios repositorioUsuarios, Comuna comuna) {
        this.repositorioUsuarios = repositorioUsuarios;
        this.comuna = comuna;
    }

    @Override
    public List<Usuario> getUsuarios() {
        return repositorioUsuarios.getUsuarios().stream()
                .filter(usuario -> comuna.equals(usuario.getComuna()))
                .collect(Collectors.toList());
    }
}
