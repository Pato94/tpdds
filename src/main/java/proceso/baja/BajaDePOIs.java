package proceso.baja;

import com.fasterxml.jackson.databind.type.CollectionType;
import origen.Mapa;
import poi.POI;
import proceso.Despertador;
import proceso.Trabajo;
import proceso.bombero.Bombero;
import utils.ResponseMapper;

import java.util.List;

/**
 * Created by Pato on 9/15/16.
 */
public class BajaDePOIs extends Trabajo {

    private final Mapa mapa;
    private final ServicioRestBaja servicioRest;
    private int elementosAfectados;

    public BajaDePOIs(ServicioRestBaja servicioRest, Mapa mapa, Despertador despertador, Bombero bombero) {
        super(despertador, bombero);
        this.servicioRest = servicioRest;
        this.mapa = mapa;
        this.elementosAfectados = 0;
    }

    @Override
    public int getElementosAfectados() {
        return elementosAfectados;
    }

    @Override
    protected void ejecutarProceso() {
        new BajaDePOIsMapper().mapResponse(servicioRest.getPOIsADarDeBaja())
                .forEach(infoDeBaja -> {
                    POI poi = buscarPOIPorId(infoDeBaja.getIdPOI());
                    if (poi != null) {
                        mapa.removerPOI(poi);
                        elementosAfectados++;
                    }
                });

    }

    private POI buscarPOIPorId(long id) {
        return BajaDePOIs.buscarPOIPorId(mapa, id);
    }

    //No esta bueno, pero en algun lado lo tengo que meter
    public static POI buscarPOIPorId(Mapa mapa, long id) {
        return mapa.todosLosPOIs()
                .stream()
                .filter(poi -> id == poi.getId()).findFirst().orElse(null);
    }

    public static class BajaDePOIsMapper extends ResponseMapper<List<InfoDeBaja>> {
        @Override
        protected CollectionType createTypeFactory() {
            return getMapper().getTypeFactory().constructCollectionType(List.class, InfoDeBaja.class);
        }
    }
}
