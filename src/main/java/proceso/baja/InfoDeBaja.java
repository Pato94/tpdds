package proceso.baja;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Pato on 9/15/16.
 */
public class InfoDeBaja {

    @JsonProperty("id")
    private long idPOI;
    @JsonProperty("fecha_baja") //Fijarse de como manejar fecha
    private String fechaBaja;

//    public InfoDeBaja(long idPOI) {
//        this(idPOI, LocalDate.now());
//    }
//
//    public InfoDeBaja(long idPOI, LocalDate fechaBaja) {
//        this.idPOI = idPOI;
//        this.fechaBaja = fechaBaja;
//    }

    public long getIdPOI() {
        return idPOI;
    }

    public String getFechaBaja() {
        return fechaBaja;
    }
}
