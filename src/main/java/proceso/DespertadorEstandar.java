package proceso;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Timer;
import java.util.TimerTask;

public class DespertadorEstandar implements Despertador {

    @Override
    public void programarPara(LocalDateTime momento, Despertable despertable) {
        programarPara(ChronoUnit.MILLIS.between(LocalDateTime.now(),
                momento), despertable);
    }

    private void programarPara(long tiempoEnMillis, Despertable despertable) {
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                despertable.despertar();
            }
        };

        new Timer().schedule(timerTask, tiempoEnMillis);
    }
}
