package proceso;

import java.time.LocalDateTime;

public interface Despertador {
    void programarPara(LocalDateTime momento, Despertable despertable);
}
