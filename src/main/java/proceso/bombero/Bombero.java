package proceso.bombero;

import proceso.ResultadoEjecucion;
import proceso.Trabajo;

/**
 * El bombero es el que se encarga de manejar las cosas cuando hay problemas
 */
public class Bombero {
    public void manejarSituacion(Exception e, Trabajo trabajoConProblemas, Trabajo.Jefe interesado) {
        if (deboReintentar()) {
            reintentar(trabajoConProblemas, interesado);
        } else {
            rendirse(trabajoConProblemas, interesado);
        }
    }

    protected void rendirse(Trabajo trabajoConProblemas, Trabajo.Jefe interesado) {
        interesado.termine(ResultadoEjecucion.fracaso(trabajoConProblemas.getElementosAfectados()));
    }

    protected void reintentar(Trabajo trabajoConProblemas, Trabajo.Jefe interesado) {
        //No se si esto esta bien checkear
        trabajoConProblemas.ejecutar(interesado);
    }

    protected boolean deboReintentar() {
        return false;
    }
}
