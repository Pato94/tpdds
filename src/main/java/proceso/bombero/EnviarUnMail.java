package proceso.bombero;

import external.ServicioDeMailing;
import proceso.Trabajo;

/**
 * Created by Pato on 8/12/16.
 */
public class EnviarUnMail extends Bombero {

    private final ServicioDeMailing servicioDeMailing;

    public EnviarUnMail(ServicioDeMailing servicioDeMailing) {
        this.servicioDeMailing = servicioDeMailing;
    }

    @Override
    protected void rendirse(Trabajo trabajoConProblemas, Trabajo.Jefe interesado) {
        servicioDeMailing.sendMailTo("complete@me.com", "Fallo una tarea", trabajoConProblemas.toString());
        super.rendirse(trabajoConProblemas, interesado);
    }
}
