package proceso.bombero;

import proceso.Trabajo;

/**
 * Created by Pato on 8/12/16.
 */
public class ReintentarNVeces extends Bombero {

    private final int cantidadDeVeces;
    private int vecesIntentadas;

    public ReintentarNVeces(int cantidadDeVecesAReintentar) {
        this.cantidadDeVeces = cantidadDeVecesAReintentar;
        this.vecesIntentadas = 0;
    }

    @Override
    protected boolean deboReintentar() {
        return cantidadDeVeces > vecesIntentadas;
    }

    @Override
    protected void reintentar(Trabajo trabajoConProblemas, Trabajo.Jefe interesado) {
        vecesIntentadas++;
        super.reintentar(trabajoConProblemas, interesado);
    }

    public int getVecesIntentadas() {
        return vecesIntentadas;
    }
}
