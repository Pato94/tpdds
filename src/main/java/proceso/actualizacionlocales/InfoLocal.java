package proceso.actualizacionlocales;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Pato on 9/15/16.
 */
public class InfoLocal {
    private String nombre;
    private List<String> tags;

    public InfoLocal(String nombre, List<String> tags) {
        this.nombre = nombre;
        this.tags = tags;
    }

    public static InfoLocal fromString(String source) {
        String[] splittedData = source.split(";");
        List<String> tags;
        if (splittedData.length > 1) { //Si viene sin tags el array solo tiene 1 grupo
            tags = Arrays.asList(splittedData[1].split(" "));
        } else {
            tags = new ArrayList<>();
        }
        return new InfoLocal(splittedData[0], tags);
    }

    public String getNombre() {
        return nombre;
    }

    public List<String> getTags() {
        return tags;
    }
}
