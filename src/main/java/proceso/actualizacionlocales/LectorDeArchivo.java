package proceso.actualizacionlocales;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

/**
 * Created by Pato on 9/15/16.
 */
public class LectorDeArchivo extends FuenteDeInfo {

    private final Path path;

    public LectorDeArchivo(Path path) {
        this.path = path;
    }

    @Override
    protected List<String> getLines() {
        try {
            return Files.readAllLines(path);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
    }
}
