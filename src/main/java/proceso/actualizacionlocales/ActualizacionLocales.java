package proceso.actualizacionlocales;

import origen.Mapa;
import poi.Local;
import proceso.Despertador;
import proceso.Trabajo;
import proceso.bombero.Bombero;

/**
 * Created by Pato on 9/15/16.
 */
public class ActualizacionLocales extends Trabajo {

    private final FuenteDeInfo fuente;
    private final Mapa mapa;
    private int elementosAfectados;

    public ActualizacionLocales(Despertador despertador, Bombero bombero, FuenteDeInfo fuente, Mapa mapa) {
        super(despertador, bombero);
        this.fuente = fuente;
        this.mapa = mapa;
        this.elementosAfectados = 0;
    }

    @Override
    public int getElementosAfectados() {
        return elementosAfectados;
    }

    @Override
    protected void ejecutarProceso() {
        fuente.getInfoLocal().forEach(infoLocal -> {
            Local local = buscarLocalPorNombre(infoLocal.getNombre());
            if (local != null) {
                Local localNuevo = new Local(1, infoLocal.getNombre(), local.getUbicacion(), local.getRubro(), local.getHorarioAtencion());
                localNuevo.setTags(infoLocal.getTags());
                mapa.modificarPOI(local, localNuevo);
                elementosAfectados++;
            }
        });
    }

    private Local buscarLocalPorNombre(String nombre) {
        return ActualizacionLocales.buscarLocalPorNombre(mapa, nombre);
    }

    //No esta bueno, pero en algun lado lo tengo que meter
    public static Local buscarLocalPorNombre(Mapa mapa, String nombre) {
        return mapa.todosLosPOIs()
                .stream().filter(poi -> poi instanceof Local).map(local -> (Local) local)
                .filter(local -> nombre.equals(local.getNombre())).findFirst().orElse(null);
    }
}
