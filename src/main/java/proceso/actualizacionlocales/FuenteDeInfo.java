package proceso.actualizacionlocales;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Pato on 9/15/16.
 */
public abstract class FuenteDeInfo {

    protected abstract List<String> getLines();

    public List<InfoLocal> getInfoLocal() {
        return getLines().stream().map(InfoLocal::fromString).collect(Collectors.toList());
    }
}
