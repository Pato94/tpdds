package proceso;

import java.time.LocalDate;

/**
 * Created by Pato on 8/12/16.
 */
public class ResultadoEjecucion {

    private final int elementosAfectados;
    private final TipoResultado tipoResultado;
    private final LocalDate fecha;

    public ResultadoEjecucion(LocalDate fecha, TipoResultado tipoResultado, int elementosAfectados) {
        this.fecha = fecha;
        this.tipoResultado = tipoResultado;
        this.elementosAfectados = elementosAfectados;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public TipoResultado getTipoResultado() {
        return tipoResultado;
    }

    public int getElementosAfectados() {
        return elementosAfectados;
    }

    public static ResultadoEjecucion exito(int elementosAfectados) {
        return new ResultadoEjecucion(LocalDate.now(), TipoResultado.EXITO, elementosAfectados);
    }


    public static ResultadoEjecucion fracaso(int elementosAfectados) {
        return new ResultadoEjecucion(LocalDate.now(), TipoResultado.FRACASO, elementosAfectados);
    }
}
