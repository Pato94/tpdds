package poi;

import consulta.ConsultaCercania;
import consulta.ConsultaDisponibilidad;
import ubicacion.Ubicacion;

import java.util.List;

public abstract class POI {

    private long id;
    protected Ubicacion ubicacion;
    private List<String> tags;

    public POI(long id, Ubicacion ubicacion) {
        this.id = id;
        this.ubicacion = ubicacion;
    }

    public boolean estaDisponible(ConsultaDisponibilidad consultaDisponibilidad) {
        return false;
    }

    public boolean coincideCon(String busqueda) {
        return tags != null &&
                tags.stream().anyMatch(tag -> tag.equalsIgnoreCase(busqueda));
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public boolean esCercano(ConsultaCercania consulta) {
        return ubicacion.distanciaA(consulta.getUbicacion()) < 500;
    }

    public Ubicacion getUbicacion() {
        return ubicacion;
    }

    public List<String> getTags() {
        return tags;
    }

    public long getId() {
        return id;
    }
}
