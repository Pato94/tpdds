package poi;

import consulta.ConsultaCercania;
import consulta.ConsultaDisponibilidad;
import horarios.DisponibilidadHoraria;
import ubicacion.Ubicacion;
import utils.StringUtils;

public class Local extends POI {

    private final String nombre;
    private final Rubro rubro;
    private final DisponibilidadHoraria horarioAtencion;

    public Local(long id, String nombre, Ubicacion ubicacion, Rubro rubro, DisponibilidadHoraria horarioAtencion) {
        super(id, ubicacion);
        this.nombre = nombre;
        this.rubro = rubro;
        this.horarioAtencion = horarioAtencion;
    }

    @Override
    public boolean esCercano(ConsultaCercania consulta) {
        return this.ubicacion.distanciaA(consulta.getUbicacion()) < this.rubro.getRadioDeCercaniaEnMetros();
    }

    @Override
    public boolean estaDisponible(ConsultaDisponibilidad consulta) {
        return this.horarioAtencion.disponible(consulta.getMomento());
    }

    @Override
    public boolean coincideCon(String busqueda) {
        return super.coincideCon(busqueda) ||
                StringUtils.containsIgnoringCasing(rubro.getNombre(), busqueda);
    }

    public String getNombre() {
        return nombre;
    }

    public Rubro getRubro() {
        return rubro;
    }

    public DisponibilidadHoraria getHorarioAtencion() {
        return horarioAtencion;
    }
}
