package poi;

import external.ServicioDTO;
import horarios.DisponibilidadCompuesta;
import horarios.DisponibilidadHoraria;
import horarios.DisponibilidadParaDia;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Servicio {

    private DisponibilidadHoraria horarioAtencion;
    private String nombre;

    public Servicio(String nombre) {
        this.nombre = nombre;
        this.horarioAtencion = new DisponibilidadHoraria() {
            @Override
            public boolean disponible(LocalDateTime momento) {
                // Disponible todo el tiempo si no se le da una DisponibilidadHoraria
                return true;
            }
        };
    }

    public Servicio(String nombre, DisponibilidadHoraria horarioAtencion) {
        this.nombre = nombre;
        this.horarioAtencion = horarioAtencion;
    }

    public boolean estaDisponible(LocalDateTime momento) {
        return this.horarioAtencion.disponible(momento);
    }

    public String getNombre() {
        return this.nombre;
    }

    public static Servicio fromServicioDTO(ServicioDTO servicioDTO) {

        LocalTime horaApertura = LocalTime.of(servicioDTO.getHorasDesde(), servicioDTO.getMinutosDesde());
        LocalTime horaCierre = LocalTime.of(servicioDTO.getHorasHasta(), servicioDTO.getMinutosHasta());

        List<DisponibilidadHoraria> diasDisponibles = Arrays.stream(servicioDTO.getDiasDeLaSemana())
                .mapToObj(num -> DayOfWeek.of(num))
                .map(dayOfWeek -> new DisponibilidadParaDia(dayOfWeek, horaApertura, horaCierre))
                .collect(Collectors.toList());

        return new Servicio(servicioDTO.getNombre(), new DisponibilidadCompuesta(diasDisponibles));
    }
}
