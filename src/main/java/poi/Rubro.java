package poi;

public class Rubro {

    private final String nombre;
    private final int radioDeCercaniaEnMetros;

    public Rubro(String nombre, int radioDeCercaniaEnMetros) {
        this.nombre = nombre;
        this.radioDeCercaniaEnMetros = radioDeCercaniaEnMetros;
    }

    public int getRadioDeCercaniaEnMetros() {
        return radioDeCercaniaEnMetros;
    }

    public String getNombre() {
        return nombre;
    }
}
