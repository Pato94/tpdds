package poi;

import consulta.ConsultaCercania;
import consulta.ConsultaDisponibilidad;
import ubicacion.Ubicacion;
import utils.StringUtils;

import java.util.List;

public class Parada extends POI {

    private List<String> lineas;

    public Parada(long id, Ubicacion ubicacion, List<String> lineas) {
        super(id, ubicacion);
        this.lineas = lineas;
    }

    @Override
    public boolean esCercano(ConsultaCercania consulta) {
        return ubicacion.distanciaA(consulta.getUbicacion()) < 100;
    }

    @Override
    public boolean estaDisponible(ConsultaDisponibilidad consultaDisponibilidad) {
        return true;
    }

    @Override
    public boolean coincideCon(final String busqueda) {
        return super.coincideCon(busqueda) || lineas.stream().anyMatch(
                linea -> StringUtils.containsIgnoringCasing(linea, busqueda)
        );
    }
}
