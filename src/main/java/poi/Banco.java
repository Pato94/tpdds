package poi;

import consulta.ConsultaDisponibilidad;
import horarios.DisponibilidadDiasHabiles;
import ubicacion.Ubicacion;

import java.time.LocalTime;
import java.util.List;

public class Banco extends POI {

    private final List<Servicio> servicios;

    public Banco(long id, Ubicacion ubicacion) {
        this(id, ubicacion, null);
    }

    public Banco(long id, Ubicacion ubicacion, List<Servicio> servicios) {
        super(id, ubicacion);
        this.servicios = servicios;
    }

    @Override
    public boolean estaDisponible(ConsultaDisponibilidad consulta) {
        if (consulta.servicioIngresado()) {
            return servicios.stream()
                    .filter(servicio -> servicio.getNombre().equals(consulta.getNombreServicio()))
                    .anyMatch(servicio -> servicio.estaDisponible(consulta.getMomento()));
        }

        DisponibilidadDiasHabiles disponibilidadPorDefecto =
                new DisponibilidadDiasHabiles(LocalTime.of(10, 0), LocalTime.of(15, 0));

        return disponibilidadPorDefecto.disponible(consulta.getMomento());
    }
}
