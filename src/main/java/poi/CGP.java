package poi;

import consulta.ConsultaCercania;
import consulta.ConsultaDisponibilidad;
import external.CentroDTO;
import ubicacion.Ubicacion;
import utils.StringUtils;

import java.util.List;
import java.util.stream.Collectors;

public class CGP extends POI {

    private List<Servicio> servicios;

    public CGP(long id, Ubicacion ubicacion, List<Servicio> servicios) {
        super(id, ubicacion);
        this.servicios = servicios;
    }

    /**
     * Es necesario que la ubicacion sepa a que comuna pertenece para realizar esta consulta
     */
    @Override
    public boolean esCercano(ConsultaCercania consulta) {
        return consulta.getComuna().contieneA(this.ubicacion);
    }

    @Override
    public boolean estaDisponible(ConsultaDisponibilidad consulta) {
        List<Servicio> serviciosAConsiderar = servicios;
        if (consulta.servicioIngresado()) {
            serviciosAConsiderar = servicios.stream()
                    .filter(servicio -> servicio.getNombre().equals(consulta.getNombreServicio()))
                    .collect(Collectors.toList());
        }

        return serviciosAConsiderar.stream()
                .anyMatch(servicio -> servicio.estaDisponible(consulta.getMomento()));
    }

    @Override
    public boolean coincideCon(String busqueda) {
        return super.coincideCon(busqueda) || servicios.stream().anyMatch(
                servicio -> StringUtils.containsIgnoringCasing(servicio.getNombre(), busqueda)
        );
    }

    /**
     * No tengo ni idea de como conseguir la ubicacion ni de si el resto de la info es relevante
     */
    public static CGP fromCentroDTO(CentroDTO centroDTO) {
        List<Servicio> servicios = centroDTO.getServicios().stream().map(Servicio::fromServicioDTO).collect(Collectors.toList());
        return new CGP(1, null, servicios);
    }

    public List<Servicio> getServicios() {
        return this.servicios;
    }
}
