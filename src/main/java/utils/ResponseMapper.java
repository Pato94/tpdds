package utils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;

import java.io.IOException;

/**
 * Created by Pato on 9/15/16.
 */
public abstract class ResponseMapper<T> {

    private ObjectMapper mapper;
    private CollectionType typeFactory;

    protected ResponseMapper() {
        mapper = new ObjectMapper();
        typeFactory = createTypeFactory();
    }

    protected abstract CollectionType createTypeFactory();

    public ObjectMapper getMapper() {
        return mapper;
    }

    public T mapResponse(String response) {
        try {
            return getMapper().readValue(response, typeFactory);
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        throw new RuntimeException("Hubo un error al parsear la respuesta");
    }
}
