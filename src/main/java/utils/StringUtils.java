package utils;

public class StringUtils {
    public static boolean containsIgnoringCasing(String big, String small) {
        return big.toLowerCase().contains(small.toLowerCase());
    }
}
