package busqueda;

import origen.OrigenDePOIs;
import poi.POI;
import usuario.Usuario;

import java.util.List;

public class BusquedaCGPs extends Busqueda {

    private final String busqueda;

    public BusquedaCGPs(Usuario usuario, String busqueda) {
        super(usuario);
        this.busqueda = busqueda;
    }

    public String getBusqueda() {
        return this.busqueda;
    }

    @Override
    public List<? extends POI> serResueltaPor(OrigenDePOIs origen) {
        return origen.resolverBusquedaCGP(this);
    }
}