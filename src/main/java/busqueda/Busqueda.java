package busqueda;

import origen.OrigenDePOIs;
import poi.POI;
import usuario.Usuario;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

public abstract class Busqueda {

    private LocalDateTime comienzo;
    private boolean resuelta;
    private long duracionEnMillis;
    private int cantidadDeResultados;
    private Usuario usuario;

    public Busqueda(Usuario usuario) {
        this.comienzo = LocalDateTime.now();
        this.usuario = usuario;
    }

    public void marcarComoResuelta(int cantidadDeResultados) {
        this.resuelta = true;
        this.duracionEnMillis = ChronoUnit.MILLIS.between(comienzo, LocalDateTime.now());
        this.cantidadDeResultados = cantidadDeResultados;
    }

    public boolean puedeSerResueltaPor(OrigenDePOIs origen) {
        return origen.busquedasPermitidas().contains(this.getClass());
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public LocalDateTime getComienzo() {
        return comienzo;
    }

    public long getDuracionEnMillis() {
        return duracionEnMillis;
    }

    public int getCantidadDeResultados() {
        return cantidadDeResultados;
    }

    public boolean seResolvio() {
        return resuelta;
    }

    public abstract List<? extends POI> serResueltaPor(OrigenDePOIs origen);
}
