package busqueda;

import origen.OrigenDePOIs;
import poi.POI;
import usuario.Usuario;

import java.util.List;

public class BusquedaBancos extends Busqueda {
    private final String nombreBanco;
    private final String nombreServicio;

    public BusquedaBancos(Usuario usuario, String nombreBanco, String nombreServicio) {
        super(usuario);
        this.nombreBanco = nombreBanco;
        this.nombreServicio = nombreServicio;
    }

    public String getNombreBanco() {
        return nombreBanco;
    }

    public String getNombreServicio() {
        return nombreServicio;
    }

    @Override
    public List<? extends POI> serResueltaPor(OrigenDePOIs origen) {
        return origen.resolverBusquedaBancos(this);
    }
}
