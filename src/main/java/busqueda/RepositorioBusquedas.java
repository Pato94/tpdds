package busqueda;

import alarma.AlarmaBusqueda;
import alarma.EnviadorDeMails;

import java.util.LinkedList;
import java.util.List;

public class RepositorioBusquedas {
    private LinkedList<Busqueda> busquedas;
    private LinkedList<AlarmaBusqueda> alarmas;

    /**
     * Podría ser una lista de alarmas pero meh
     */
    public RepositorioBusquedas(EnviadorDeMails enviador) {
        this.busquedas = new LinkedList<>();
        this.alarmas = new LinkedList<>();
        this.alarmas.add(enviador);
    }

    public void agregarBusqueda(Busqueda busqueda) {
        this.alarmas.stream().forEach(alarma -> alarma.avisarSiDebe(busqueda));
        this.busquedas.add(busqueda);
    }

    public void agregarAlarma(AlarmaBusqueda alarma) {
        this.alarmas.add(alarma);
    }

    public List<Busqueda> getBusquedas() {
        return busquedas;
    }
}
