package busqueda;

import origen.OrigenDePOIs;
import poi.POI;
import usuario.Usuario;

import java.util.List;

public class BusquedaEnMapa extends Busqueda {
    private String busqueda;

    public BusquedaEnMapa(Usuario usuario, String busqueda) {
        super(usuario);
        this.busqueda = busqueda;
    }

    public String getBusqueda() {
        return this.busqueda;
    }

    @Override
    public List<? extends POI> serResueltaPor(OrigenDePOIs origen) {
        return origen.resolverBusquedaMapa(this);
    }
}