package busqueda;

import origen.OrigenDePOIs;
import poi.POI;

import java.util.List;
import java.util.stream.Collectors;

public class Buscador {
    private List<OrigenDePOIs> origenes;
    private RepositorioBusquedas repo;

    public Buscador(RepositorioBusquedas repo, List<OrigenDePOIs> origenes) {
        this.repo = repo;
        this.origenes = origenes;
    }

    public List<POI> buscar(Busqueda busqueda) {
        List<POI> resultado = origenes.stream()
                .filter(origen -> busqueda.puedeSerResueltaPor(origen))
                .flatMap(origen -> busqueda.serResueltaPor(origen).stream())
                .collect(Collectors.toList());

        busqueda.marcarComoResuelta(resultado.size());
        this.repo.agregarBusqueda(busqueda);

        return resultado;
    }
}
