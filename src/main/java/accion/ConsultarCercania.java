package accion;

import consulta.ConsultaCercania;
import poi.POI;
import ubicacion.Comuna;
import ubicacion.Ubicacion;
import usuario.Usuario;

public class ConsultarCercania extends AccionSistema {

    private POI poi;
    private Ubicacion ubicacion;
    private Comuna comuna;

    public ConsultarCercania(POI poi, Ubicacion ubicacion, Comuna comuna) {
        this.poi = poi;
        this.ubicacion = ubicacion;
        this.comuna = comuna;
    }

    @Override
    public void realizar(Usuario usuario) {
        poi.esCercano(new ConsultaCercania(ubicacion, comuna));
    }

}
