package accion;

import consulta.ConsultaDisponibilidad;
import poi.POI;
import usuario.Usuario;

import java.time.LocalDateTime;

public class ConsultarDisponibilidad extends AccionSistema {

    private final POI poi;
    private final LocalDateTime momento;
    private final String servicio;

    public ConsultarDisponibilidad(POI poi, LocalDateTime momento, String servicio) {
        this.poi = poi;
        this.momento = momento;
        this.servicio = servicio;
    }

    @Override
    public void realizar(Usuario usuario) {
        poi.estaDisponible(new ConsultaDisponibilidad(this.momento, this.servicio));
    }
}
