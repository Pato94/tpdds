package accion;

import busqueda.Buscador;
import busqueda.BusquedaEnMapa;
import usuario.Usuario;

public class BuscarEnMapa extends AccionSistema {

    private Buscador buscador;
    private String busqueda;

    public BuscarEnMapa(Buscador buscador, String busqueda) {
        this.buscador = buscador;
        this.busqueda = busqueda;
    }

    @Override
    public void realizar(Usuario usuario) {
        buscador.buscar(new BusquedaEnMapa(usuario, busqueda));
    }
}
