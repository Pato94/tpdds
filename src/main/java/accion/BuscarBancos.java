package accion;

import busqueda.Buscador;
import busqueda.BusquedaBancos;
import usuario.Usuario;

public class BuscarBancos extends AccionSistema {

    private Buscador buscador;
    private String nombreBanco;
    private String nombreServicio;

    public BuscarBancos(Buscador buscador, String nombreBanco, String nombreServicio) {
        this.buscador = buscador;
        this.nombreBanco = nombreBanco;
        this.nombreServicio = nombreServicio;
    }

    @Override
    public void realizar(Usuario usuario) {
        buscador.buscar(new BusquedaBancos(usuario, nombreBanco, nombreServicio));
    }

}
