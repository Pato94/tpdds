package accion;

import busqueda.Buscador;
import busqueda.BusquedaCGPs;
import usuario.Usuario;

public class BuscarCGPs extends AccionSistema {

    private Buscador buscador;
    private String busqueda;

    public BuscarCGPs(Buscador buscador, String busqueda) {
        this.buscador = buscador;
        this.busqueda = busqueda;
    }

    @Override
    public void realizar(Usuario usuario) {
        buscador.buscar(new BusquedaCGPs(usuario, busqueda));
    }
}
