package accion;

import usuario.Usuario;

public abstract class AccionSistema {
    /**
     * Es complicado manejar el tema del tipo de retorno
     */
    public abstract void realizar(Usuario usuario);
}