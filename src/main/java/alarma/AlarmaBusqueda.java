package alarma;

import busqueda.Busqueda;

public abstract class AlarmaBusqueda {
    //Un toco de logica kek
    public void avisarSiDebe(Busqueda busqueda) {
        if (deboAvisar(busqueda)) {
            avisar(busqueda);
        }
    }

    public abstract void avisar(Busqueda busqueda);

    protected abstract boolean deboAvisar(Busqueda busqueda);
}
