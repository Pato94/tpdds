package alarma;

import busqueda.Busqueda;
import external.ServicioDeMailing;

public class EnviadorDeMails extends AlarmaBusqueda {

    protected ServicioDeMailing servicio;
    protected long tolerancia;
    protected String email;

    public EnviadorDeMails(ServicioDeMailing mailing, long toleranciaEnMillis, String email) {
        this.servicio = mailing;
        this.tolerancia = toleranciaEnMillis;
        this.email = email;
    }

    @Override
    public void avisar(Busqueda busqueda) {
        this.servicio.sendMailTo(this.email, "Una busqueda demoro demasiado", busqueda.toString());
    }

    @Override
    protected boolean deboAvisar(Busqueda busqueda) {
        return busqueda.getDuracionEnMillis() > tolerancia;
    }

}
