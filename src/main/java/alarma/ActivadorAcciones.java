package alarma;

import accion.AccionSistema;
import busqueda.Busqueda;
import proceso.Trabajo;
import proceso.accionesusuarios.ActivarAcciones;
import proceso.accionesusuarios.ListaDeterminada;
import proceso.bombero.Bombero;

import java.util.Collections;
import java.util.List;

/**
 * Created by Pato on 9/16/16.
 */
public class ActivadorAcciones extends AlarmaBusqueda {

    private final Trabajo.Jefe jefe;
    private final List<Class<? extends AccionSistema>> acciones;

    public ActivadorAcciones(Trabajo.Jefe jefe, List<Class<? extends AccionSistema>> acciones) {
        this.jefe = jefe;
        this.acciones = acciones;
    }

    @Override
    public void avisar(Busqueda busqueda) {
        ActivarAcciones activarAcciones = new ActivarAcciones(
                new ListaDeterminada(Collections.singletonList(busqueda.getUsuario())),
                acciones,
                null,
                new Bombero()
        );

        activarAcciones.ejecutarAhora(jefe);
    }

    @Override
    protected boolean deboAvisar(Busqueda busqueda) {
        return true;
    }
}
