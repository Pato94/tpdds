package external;

public interface ServicioDeMailing {

    void sendMailTo(String email, String subject, String content);

}
