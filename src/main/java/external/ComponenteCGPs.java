package external;

import java.util.List;

public interface ComponenteCGPs {
    public List<CentroDTO> buscarCGPs(String busqueda);
}