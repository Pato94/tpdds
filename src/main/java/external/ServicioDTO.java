package external;

public class ServicioDTO {

    private final String nombre;
    private final int[] diasDeLaSemana;
    private final int horasDesde;
    private final int minutosDesde;
    private final int horasHasta;
    private final int minutosHasta;

    public ServicioDTO(String nombre, int[] diasDeLaSemana, int horasDesde, int minutosDesde, int horasHasta, int minutosHasta) {
        this.nombre = nombre;
        this.diasDeLaSemana = diasDeLaSemana;
        this.horasDesde = horasDesde;
        this.minutosDesde = minutosDesde;
        this.horasHasta = horasHasta;
        this.minutosHasta = minutosHasta;
    }

    public String getNombre() {
        return nombre;
    }

    public int[] getDiasDeLaSemana() {
        return diasDeLaSemana;
    }

    public int getHorasDesde() {
        return horasDesde;
    }

    public int getMinutosDesde() {
        return minutosDesde;
    }

    public int getHorasHasta() {
        return horasHasta;
    }

    public int getMinutosHasta() {
        return minutosHasta;
    }
}
