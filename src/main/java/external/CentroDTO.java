package external;

import java.util.List;

public class CentroDTO {

    private final int numeroDeComuna;
    private final String zonasParaComuna;
    private final String nombreDeDirector;
    private final String domicilio;
    private final String telefono;
    private final List<ServicioDTO> servicios;

    public CentroDTO(int numeroDeComuna, String zonasParaComuna, String nombreDeDirector, String domicilio, String telefono, List<ServicioDTO> servicios) {
        this.numeroDeComuna = numeroDeComuna;
        this.zonasParaComuna = zonasParaComuna;
        this.nombreDeDirector = nombreDeDirector;
        this.domicilio = domicilio;
        this.telefono = telefono;
        this.servicios = servicios;
    }

    public int getNumeroDeComuna() {
        return numeroDeComuna;
    }

    public String getZonasParaComuna() {
        return zonasParaComuna;
    }

    public String getNombreDeDirector() {
        return nombreDeDirector;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public String getTelefono() {
        return telefono;
    }

    public List<ServicioDTO> getServicios() {
        return servicios;
    }
}
