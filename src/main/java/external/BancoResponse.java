package external;

import com.fasterxml.jackson.annotation.JsonProperty;
import poi.Banco;
import poi.Servicio;
import ubicacion.Ubicacion;

import java.util.List;
import java.util.stream.Collectors;

public class BancoResponse {
    @JsonProperty("banco")
    private String banco;
    @JsonProperty("x")
    private double x;
    @JsonProperty("y")
    private double y;
    @JsonProperty("servicios")
    private List<String> servicios;
    @JsonProperty("sucursal")
    private String sucursal;
    @JsonProperty("gerente")
    private String gerente;

    public Banco toBanco() {
        List<Servicio> servicios = this.servicios.stream().map(servicio -> new Servicio(servicio)).collect(Collectors.toList());
        return new Banco(1, new Ubicacion(this.x, this.y), servicios);
    }
}
