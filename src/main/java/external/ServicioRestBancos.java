package external;

/**
 * Es incomodo pero hace falta para despues poder testear
 */
public interface ServicioRestBancos {
    public String buscarBancos(String nombreBanco, String nombreServicio);
}