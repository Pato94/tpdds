package fixture;

import dummy.POIStub;
import origen.Mapa;
import poi.*;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class FixturePOIS {

    public Banco bancoConAtencionAlCliente;
    public Banco bancoConTodosLosDepositos;
    public CGP gcpConLimpieza;
    public CGP gcpConLimpiezaYDeposito;
    public Local localDeMuebles;
    public Local localDeMadera;
    public Local localDePanchos;
    public Local localDePanchosDeCarlitos;
    public Parada paradaDel114;
    public Mapa mapaConTodosLosPOIs;
    public POIStub poiStub;

    public FixturePOIS() {
        setUpPOIs();
        setUpMapa();
    }

    private void setUpMapa() {
        mapaConTodosLosPOIs = new Mapa(
                //Usar un linked list nos salva del UnsupportedOperationException
                new LinkedList<>(Arrays.asList(
                        bancoConAtencionAlCliente,
                        bancoConTodosLosDepositos,
                        gcpConLimpieza,
                        gcpConLimpiezaYDeposito,
                        localDeMadera,
                        localDeMuebles,
                        localDePanchos,
                        localDePanchosDeCarlitos,
                        paradaDel114
                ))
        );
    }

    private void setUpPOIs() {
        FixtureHorarios fixtureHorario = new FixtureHorarios();
        FixtureUbicacion fixtureUbicacion = new FixtureUbicacion();

        Servicio atencionAlCliente = new Servicio("Atencion al cliente", fixtureHorario.disponibilidadTodosLosDiasDe9A18);
        Servicio contaduria = new Servicio("Contaduria", fixtureHorario.disponibilidadParaLunesDe9A10);
        Servicio deposito = new Servicio("Deposito", fixtureHorario.disponibilidadParaLunesDe10A11);
        Servicio limpieza = new Servicio("Limpieza", fixtureHorario.disponibilidadParaLunesYMiercolesDe9A10);

        Rubro muebleria = new Rubro("Muebleria", 500);
        Rubro carpinteria = new Rubro("Carpinteria", 1500);
        Rubro pancheria = new Rubro("Pancheria", 200);

        List<String> tagSet1 = Arrays.asList("groso", "bueno", "buenisimo");
        List<String> tagSet2 = Arrays.asList("lento", "pesimo");
        List<String> tagSet3 = Arrays.asList("malo");
        List<String> tagSet4 = Arrays.asList("rapidisimo");

        bancoConAtencionAlCliente = new Banco(1, fixtureUbicacion.ubicacion, Arrays.asList(atencionAlCliente));
        bancoConTodosLosDepositos = new Banco(2, fixtureUbicacion.ubicacionAMenosDeUnaCuadra, Arrays.asList(atencionAlCliente, contaduria, deposito, limpieza));

        gcpConLimpieza = new CGP(3, fixtureUbicacion.ubicacion, Arrays.asList(limpieza));
        gcpConLimpiezaYDeposito = new CGP(4, fixtureUbicacion.ubicacionAMasDe5Cuadras, Arrays.asList(limpieza, deposito));

        localDeMuebles = new Local(5, "Muebles Locos", fixtureUbicacion.ubicacionFueraDeLaComuna, muebleria, fixtureHorario.disponibilidadParaLunesYMiercolesDe9A10);
        localDeMadera = new Local(6, "Carpinteria El tano", fixtureUbicacion.ubicacionAMenosDeUnaCuadra, carpinteria, fixtureHorario.disponibilidadParaLunesDe9A18);
        localDePanchos = new Local(7, "Pancheria", fixtureUbicacion.ubicacionAMenosDe5Cuadras, pancheria, fixtureHorario.disponibilidadTodosLosDiasDe9A18);
        localDePanchosDeCarlitos = new Local(8, "Lo de Carlitos", fixtureUbicacion.ubicacion, pancheria, fixtureHorario.disponibilidadTodosLosDiasDe9A18);

        paradaDel114 = new Parada(9, fixtureUbicacion.ubicacion, Arrays.asList("114"));
        poiStub = new POIStub(fixtureUbicacion.ubicacion);

        bancoConAtencionAlCliente.setTags(tagSet1);
        bancoConTodosLosDepositos.setTags(tagSet2);
        gcpConLimpieza.setTags(tagSet3);
        gcpConLimpiezaYDeposito.setTags(tagSet4);
        localDeMuebles.setTags(tagSet1);
        localDeMadera.setTags(tagSet2);
        localDePanchos.setTags(tagSet3);
        localDePanchosDeCarlitos.setTags(tagSet4);
        paradaDel114.setTags(tagSet1);
    }
}
