package fixture;

import ubicacion.Comuna;
import ubicacion.Ubicacion;

import java.util.Arrays;

public class FixtureUbicacion {
    public Ubicacion ubicacion;
    public Ubicacion ubicacionAMenosDeUnaCuadra;
    public Ubicacion ubicacionAMenosDe5Cuadras;
    public Ubicacion ubicacionAMasDe5Cuadras;
    public Ubicacion ubicacionAMasDeUnaCuadra;
    public Comuna comuna;
    public Ubicacion ubicacionDentroDeLaComuna;
    public Ubicacion ubicacionFueraDeLaComuna;

    public FixtureUbicacion() {
        setUpComuna();
        setUpUbicaciones();
    }

    private void setUpUbicaciones() {
        ubicacion = new Ubicacion(-34.655662, -58.464503);
        ubicacionAMenosDeUnaCuadra = new Ubicacion(-34.655290, -58.464954); //58m
        ubicacionDentroDeLaComuna = new Ubicacion(-34.655290, -58.464954); //58m
        ubicacionAMenosDe5Cuadras = new Ubicacion(-34.655290, -58.464954); //58m
        ubicacionAMasDe5Cuadras = new Ubicacion(-34.653789, -58.481045); //1527m
        ubicacionFueraDeLaComuna = new Ubicacion(-33.653789, -58.481045); //Bastante
        ubicacionAMasDeUnaCuadra = new Ubicacion(-34.657450, -58.467560); //343m
    }

    private void setUpComuna() {
        Ubicacion vertice1 = new Ubicacion(-34.0, -59.0);
        Ubicacion vertice2 = new Ubicacion(-35.0, -58.0);
        Ubicacion vertice3 = new Ubicacion(-35.0, -59.0);
        Ubicacion vertice4 = new Ubicacion(-34.0, -58.0); //No nos pongamos finos
        comuna = new Comuna("Test comuna", Arrays.asList(vertice1, vertice2, vertice3, vertice4));
    }
}