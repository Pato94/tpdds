package fixture;

import horarios.DisponibilidadCompuesta;
import horarios.DisponibilidadDiasHabiles;
import horarios.DisponibilidadParaDia;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Arrays;

public class FixtureHorarios {

    public LocalDateTime lunes0901;
    public LocalDateTime lunes1001;
    public LocalDateTime martes0901;
    public LocalDateTime miercoles0901;
    public LocalDateTime jueves0901;
    public LocalDateTime viernes0901;
    public LocalDateTime sabado0901;
    public LocalDateTime domingo0901;
    public LocalDateTime martes1001;
    public DisponibilidadParaDia disponibilidadParaMiercolesDe9A10;
    public DisponibilidadParaDia disponibilidadParaLunesDe9A10;
    public DisponibilidadCompuesta disponibilidadParaLunesYMiercolesDe9A10;
    public DisponibilidadParaDia disponibilidadParaLunesDe10A11;
    public DisponibilidadDiasHabiles disponibilidadTodosLosDiasDe9A18;
    public DisponibilidadParaDia disponibilidadParaLunesDe9A18;

    public FixtureHorarios() {
        setUpHorarios();
        setUpDisponibilidades();
    }

    private void setUpDisponibilidades() {
        disponibilidadParaLunesDe9A10 =
                new DisponibilidadParaDia(DayOfWeek.MONDAY, LocalTime.of(9, 0), LocalTime.of(10, 0));
        disponibilidadParaMiercolesDe9A10 =
                new DisponibilidadParaDia(DayOfWeek.WEDNESDAY, LocalTime.of(9, 0), LocalTime.of(10, 0));
        disponibilidadParaLunesYMiercolesDe9A10 =
                new DisponibilidadCompuesta(
                        Arrays.asList(disponibilidadParaLunesDe9A10, disponibilidadParaMiercolesDe9A10));
        disponibilidadParaLunesDe10A11 =
                new DisponibilidadParaDia(DayOfWeek.MONDAY, LocalTime.of(10, 0), LocalTime.of(11, 0));
        disponibilidadTodosLosDiasDe9A18 =
                new DisponibilidadDiasHabiles(LocalTime.of(9, 0), LocalTime.of(18, 0));
        disponibilidadParaLunesDe9A18 =
                new DisponibilidadParaDia(DayOfWeek.MONDAY, LocalTime.of(9, 0), LocalTime.of(18, 0));
    }

    public void setUpHorarios() {
        lunes0901 = LocalDateTime.of(2016, 5, 9, 9, 1);
        lunes1001 = LocalDateTime.of(2016, 5, 9, 10, 1);
        martes0901 = LocalDateTime.of(2016, 5, 10, 9, 1);
        martes1001 = LocalDateTime.of(2016, 5, 10, 10, 1);
        miercoles0901 = LocalDateTime.of(2016, 5, 11, 9, 1);
        jueves0901 = LocalDateTime.of(2016, 5, 12, 9, 1);
        viernes0901 = LocalDateTime.of(2016, 5, 13, 9, 1);
        sabado0901 = LocalDateTime.of(2016, 5, 14, 9, 1);
        domingo0901 = LocalDateTime.of(2016, 5, 15, 9, 1);
    }
}
