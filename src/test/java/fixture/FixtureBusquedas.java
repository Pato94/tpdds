package fixture;

import busqueda.BusquedaBancos;
import busqueda.BusquedaCGPs;
import busqueda.BusquedaEnMapa;
import dummy.BusquedaConFecha;
import dummy.BusquedaQueAgregaDelay;
import dummy.BusquedaQueNadieJuna;
import dummy.PatoElGranTesteador;

import java.time.LocalDateTime;

public class FixtureBusquedas {

    public BusquedaConFecha busqueda070716_0707;
    public BusquedaConFecha busqueda070716_0807;
    public BusquedaConFecha busqueda070716_0907;
    public BusquedaConFecha busqueda080716_0707;
    public BusquedaConFecha busqueda080716_0807;
    public BusquedaConFecha busqueda090716_0707;
    public BusquedaConFecha busqueda090716_0807;
    public BusquedaQueAgregaDelay busquedaQueBlockea100ms;
    public BusquedaQueAgregaDelay busquedaQueBlockea200ms;
    public BusquedaQueAgregaDelay busquedaQueBlockea300ms;
    public BusquedaQueNadieJuna busquedaQueNadieJuna;
    public BusquedaCGPs busquedaCGPs;
    public BusquedaEnMapa busquedaMapa;
    public BusquedaBancos busquedaBancos;

    public FixtureBusquedas() {
        PatoElGranTesteador pato = new PatoElGranTesteador("topa", null);
        busqueda070716_0707 = new BusquedaConFecha(LocalDateTime.of(2016, 7, 7, 7, 7));
        busqueda070716_0807 = new BusquedaConFecha(LocalDateTime.of(2016, 7, 7, 8, 7));
        busqueda070716_0907 = new BusquedaConFecha(LocalDateTime.of(2016, 7, 7, 9, 7));
        busqueda080716_0707 = new BusquedaConFecha(LocalDateTime.of(2016, 7, 8, 7, 7));
        busqueda080716_0807 = new BusquedaConFecha(LocalDateTime.of(2016, 7, 8, 8, 7));
        busqueda090716_0707 = new BusquedaConFecha(LocalDateTime.of(2016, 7, 9, 7, 7));
        busqueda090716_0807 = new BusquedaConFecha(LocalDateTime.of(2016, 7, 9, 8, 7));
        busquedaQueBlockea100ms = new BusquedaQueAgregaDelay(100);
        busquedaQueBlockea200ms = new BusquedaQueAgregaDelay(200);
        busquedaQueBlockea300ms = new BusquedaQueAgregaDelay(300);
        busquedaQueNadieJuna = new BusquedaQueNadieJuna();
        busquedaCGPs = new BusquedaCGPs(pato, "");
        busquedaMapa = new BusquedaEnMapa(pato, "");
        busquedaBancos = new BusquedaBancos(pato, "", "");
    }
}
