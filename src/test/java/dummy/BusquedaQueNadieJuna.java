package dummy;

import busqueda.Busqueda;
import fixture.FixturePOIS;
import origen.OrigenDePOIs;
import poi.POI;

import java.util.Arrays;
import java.util.List;

/**
 * Este mock te trae 2 pois ignorando el origen
 * Sin embargo nunca va a ser delegada a un origen
 * Porque la idea es que ninguno la tenga en "busquedasPermitidas"
 */
public class BusquedaQueNadieJuna extends Busqueda {
    private List<POI> respuesta;

    public BusquedaQueNadieJuna() {
        super(new PatoElGranTesteador(null));
        FixturePOIS fixturePOIs = new FixturePOIS();
        this.respuesta = Arrays.asList(
                fixturePOIs.bancoConAtencionAlCliente,
                fixturePOIs.paradaDel114
        );
    }

    @Override
    public List<? extends POI> serResueltaPor(OrigenDePOIs origen) {
        return respuesta;
    }
}
