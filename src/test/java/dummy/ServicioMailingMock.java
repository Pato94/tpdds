package dummy;

import external.ServicioDeMailing;

import java.util.LinkedList;

public class ServicioMailingMock implements ServicioDeMailing {

    private final LinkedList<MailData> mailsEnviados;

    public ServicioMailingMock() {
        this.mailsEnviados = new LinkedList<>();
    }

    @Override
    public void sendMailTo(String email, String subject, String content) {
        this.mailsEnviados.add(new MailData(email, subject, content));
    }

    public LinkedList<MailData> getMailsEnviados() {
        return mailsEnviados;
    }

    public class MailData {
        private final String email;
        private final String subject;
        private final String content;

        public MailData(String email, String subject, String content) {
            this.email = email;
            this.subject = subject;
            this.content = content;
        }

        public String getEmail() {
            return email;
        }

        public String getSubject() {
            return subject;
        }

        public String getContent() {
            return content;
        }
    }
}
