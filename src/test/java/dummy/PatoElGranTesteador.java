package dummy;

import ubicacion.Comuna;
import usuario.Usuario;

public class PatoElGranTesteador extends Usuario {
    public PatoElGranTesteador(Comuna comuna) {
        this("pato", comuna);
    }

    public PatoElGranTesteador(String nombre, Comuna comuna) {
        super(nombre, "pato@eltesterloco.com", comuna);
    }
}
