package dummy;

import external.ServicioRestBancos;

/**
 * Agarre el json del tp y lo pegue aca
 */
public class ServicioRestBancosMock implements ServicioRestBancos {

    @Override
    public String buscarBancos(String nombreBanco, String nombreServicio) {
        return "[" + "{ \"banco\": \"Banco de la Plaza\"," + "\"x\": -35.9338322," + "\"y\": 72.348353,"
                + "\"sucursal\": \"Avellaneda\"," + "\"gerente\": \"Javier Loeschbor\","
                + "\"servicios\": [ \"cobro cheques\", \"depósitos\", \"extracciones\", \"transferencias\", \"créditos\", \"\", \"\", \"\" ]"
                + " }," + "{ \"banco\": \"Banco de la Plaza\"," + "\"x\": -35.9338322," + "\"y\": 72.348353,"
                + "\"sucursal\": \"Avellaneda\"," + "\"gerente\": \"Javier Loeschbor\","
                + "\"servicios\": [ \"cobro cheques\", \"depósitos\", \"extracciones\", \"transferencias\", \"créditos\", \"\", \"\", \"\" ]"
                + " }" + "]";
    }
}
