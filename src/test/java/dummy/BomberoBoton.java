package dummy;

import proceso.Trabajo;
import proceso.bombero.Bombero;

/**
 * Created by Pato on 9/15/16.
 */
public class BomberoBoton extends Bombero {

    private Exception lastException;
    private int situacionesManejadas;

    public BomberoBoton() {
        this.situacionesManejadas = 0;
    }

    @Override
    public void manejarSituacion(Exception e, Trabajo trabajoConProblemas, Trabajo.Jefe interesado) {
        super.manejarSituacion(e, trabajoConProblemas, interesado);
        this.lastException = e;
        this.situacionesManejadas++;
    }

    public Exception getLastException() {
        return lastException;
    }

    public int getSituacionesManejadas() {
        return situacionesManejadas;
    }
}
