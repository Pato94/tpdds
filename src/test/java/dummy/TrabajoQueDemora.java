package dummy;

import proceso.DespertadorEstandar;
import proceso.Trabajo;
import proceso.bombero.Bombero;

public class TrabajoQueDemora extends Trabajo {

    private long timeToDelay;

    public TrabajoQueDemora(long timeToDelayInMillis) {
        super(new DespertadorEstandar(), new Bombero());
        this.timeToDelay = timeToDelayInMillis;
    }

    @Override
    public int getElementosAfectados() {
        return 0;
    }

    @Override
    protected void ejecutarProceso() {
        try {
            Thread.sleep(timeToDelay);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
