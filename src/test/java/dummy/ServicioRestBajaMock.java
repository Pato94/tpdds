package dummy;

import proceso.baja.ServicioRestBaja;

/**
 * Created by Pato on 9/16/16.
 */
public class ServicioRestBajaMock implements ServicioRestBaja {
    @Override
    public String getPOIsADarDeBaja() {
        return "[\n" +
                "  {\n" +
                "    \"id\": 1,\n" +
                "    \"fecha_baja\": \"04/06/1994\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"id\": 2,\n" +
                "    \"fecha_baja\": \"05/06/1994\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"id\": 99,\n" + //La idea es que este id no este en el mapa y sea ignorado
                "    \"fecha_baja\": \"07/06/1994\"\n" +
                "  }\n" +
                "]";
    }
}
