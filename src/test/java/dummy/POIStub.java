package dummy;

import consulta.ConsultaCercania;
import consulta.ConsultaDisponibilidad;
import poi.POI;
import ubicacion.Ubicacion;

public class POIStub extends POI {

    public int esCercanoTimesCalled = 0;
    public int estaDisponibleTimesCalled = 0;

    public POIStub(Ubicacion ubicacion) {
        super(1, ubicacion);
    }

    @Override
    public boolean esCercano(ConsultaCercania consulta) {
        esCercanoTimesCalled++;
        return true;
    }

    @Override
    public boolean estaDisponible(ConsultaDisponibilidad consulta) {
        estaDisponibleTimesCalled++;
        return true;
    }
}
