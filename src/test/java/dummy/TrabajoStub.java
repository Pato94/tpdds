package dummy;

import proceso.DespertadorEstandar;
import proceso.Trabajo;
import proceso.bombero.Bombero;

public class TrabajoStub extends Trabajo {

    private boolean realizado;

    public TrabajoStub() {
        super(new DespertadorEstandar(), new Bombero());
        this.realizado = false;
    }

    @Override
    public int getElementosAfectados() {
        return 0;
    }

    @Override
    protected void ejecutarProceso() {
        this.realizado = true;
    }

    public boolean seRealizo() {
        return realizado;
    }
}
