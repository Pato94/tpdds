package dummy;

import proceso.actualizacionlocales.FuenteDeInfo;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Pato on 9/15/16.
 */
public class FuenteDeInfoStub extends FuenteDeInfo {

    private final LinkedList<String> datos;

    public FuenteDeInfoStub() {
        this.datos = new LinkedList<>();
    }

    @Override
    protected List<String> getLines() {
        return datos;
    }

    public void agregarDato(String dato) {
        this.datos.add(dato);
    }
}
