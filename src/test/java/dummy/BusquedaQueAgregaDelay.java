package dummy;

import busqueda.Busqueda;
import origen.OrigenDePOIs;
import poi.POI;

import java.util.List;

/**
 * Esta busqueda agrega una demora con fines de testeo
 * El delay deseado es recibido en el constructor
 * --IMPORTANTE--
 * La aplicacion del delay se realiza en ">>marcarComoResuelta"
 */
public class BusquedaQueAgregaDelay extends Busqueda {

    private int delayInMillis;

    public BusquedaQueAgregaDelay(int delayInMillis) {
        super(new PatoElGranTesteador("topa", null));
        this.delayInMillis = delayInMillis;
    }

    //Nunca va a ser llamado
    @Override
    public List<? extends POI> serResueltaPor(OrigenDePOIs origen) {
        return null;
    }

    @Override
    public void marcarComoResuelta(int cantidadDeResultados) {
        try {
            Thread.sleep(delayInMillis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        super.marcarComoResuelta(cantidadDeResultados);
    }
}
