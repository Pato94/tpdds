package dummy;

import external.CentroDTO;
import external.ComponenteCGPs;
import external.ServicioDTO;

import java.util.Arrays;
import java.util.List;

/**
 * Este mock te devuelve 3 centroDTOs sin importar la busqueda que ingreses
 * En realidad es un Mock j3j3
 */
public class ComponenteCGPsMock implements ComponenteCGPs {
    public ComponenteCGPsMock() {
    }

    @Override
    public List<CentroDTO> buscarCGPs(String busqueda) {
        return Arrays.asList(createCentroDTO1(), createCentroDTO2(), createCentroDTO3());
    }

    public CentroDTO createCentroDTO1() {
        ServicioDTO servicioDTO1 = new ServicioDTO("Test 1", new int[]{1, 2, 3, 4, 5}, 9, 0, 18, 0);
        ServicioDTO servicioDTO2 = new ServicioDTO("Test 2", new int[]{1, 2, 3}, 9, 0, 18, 0);
        ServicioDTO servicioDTO3 = new ServicioDTO("Test 3", new int[]{3}, 9, 0, 11, 0);
        return new CentroDTO(1, null, null, null, null, Arrays.asList(servicioDTO1, servicioDTO2, servicioDTO3));
    }

    public CentroDTO createCentroDTO2() {
        ServicioDTO servicioDTO2 = new ServicioDTO("Test 2", new int[]{1, 2, 3}, 9, 0, 18, 0);
        ServicioDTO servicioDTO3 = new ServicioDTO("Test 3", new int[]{3}, 9, 0, 11, 0);
        return new CentroDTO(2, null, null, null, null, Arrays.asList(servicioDTO2, servicioDTO3));
    }

    public CentroDTO createCentroDTO3() {
        ServicioDTO servicioDTO1 = new ServicioDTO("Test 1", new int[]{1, 2, 3, 4, 5}, 9, 0, 18, 0);
        return new CentroDTO(3, null, null, null, null, Arrays.asList(servicioDTO1));
    }
}
