package dummy;

import proceso.DespertadorEstandar;
import proceso.Trabajo;
import proceso.bombero.Bombero;

/**
 * Created by Pato on 8/12/16.
 */
public class TrabajoQueFallaNVeces extends Trabajo {

    private final int vecesAFallar;
    private int vecesFalladas;

    public TrabajoQueFallaNVeces(int vecesAFallar, Bombero bombero) {
        super(new DespertadorEstandar(), bombero);
        this.vecesAFallar = vecesAFallar;
        this.vecesFalladas = 0;
    }

    @Override
    public int getElementosAfectados() {
        return 0;
    }

    @Override
    protected void ejecutarProceso() {
        if (++vecesFalladas > vecesAFallar) return;
        throw new RuntimeException("It's an ugly job, but somebody has to do it!");
    }

    public int getVecesQueFallo() {
        return vecesFalladas;
    }
}
