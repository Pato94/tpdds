package dummy;

import busqueda.Busqueda;
import origen.OrigenDePOIs;
import poi.POI;

import java.time.LocalDateTime;
import java.util.List;

public class BusquedaConFecha extends Busqueda {

    private LocalDateTime comienzoCustom;

    public BusquedaConFecha(LocalDateTime comienzo) {
        super(new PatoElGranTesteador(null));
        this.comienzoCustom = comienzo;
    }

    @Override
    public LocalDateTime getComienzo() {
        return comienzoCustom;
    }

    @Override
    public List<? extends POI> serResueltaPor(OrigenDePOIs origen) {
        // TODO Auto-generated method stub
        return null;
    }
}
