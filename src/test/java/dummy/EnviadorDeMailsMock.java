package dummy;

import alarma.EnviadorDeMails;

public class EnviadorDeMailsMock extends EnviadorDeMails {

    /**
     * Este mock tiene 150ms de tolerancia para no tener que ir hacerme un cafe
     * Cada vez que corro los tests
     */
    public EnviadorDeMailsMock() {
        super(new ServicioMailingMock(), 150, "pato@eltesterloco.com");
        // TODO Auto-generated constructor stub
    }

    public ServicioMailingMock getServicioMailing() {
        return (ServicioMailingMock) this.servicio;
    }
}
