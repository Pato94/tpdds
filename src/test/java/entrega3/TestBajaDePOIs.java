package entrega3;

import dummy.ServicioRestBajaMock;
import fixture.FixturePOIS;
import org.junit.Before;
import org.junit.Test;
import origen.Mapa;
import proceso.DespertadorEstandar;
import proceso.Sindicalista;
import proceso.baja.BajaDePOIs;
import proceso.bombero.Bombero;
import utils.Demorador;

import static org.junit.Assert.assertEquals;

/**
 * Created by Pato on 9/15/16.
 */
public class TestBajaDePOIs {

    private Mapa mapa;
    private ServicioRestBajaMock servicioMock;

    @Before
    public void setUp() {
        this.mapa = new FixturePOIS().mapaConTodosLosPOIs;
        this.servicioMock = new ServicioRestBajaMock();
    }

    @Test //Hay que testear
    public void puedoParsearLaRespuesta() {
        BajaDePOIs.BajaDePOIsMapper mapper = new BajaDePOIs.BajaDePOIsMapper();
        assertEquals(3, mapper.mapResponse(servicioMock.getPOIsADarDeBaja()).size());
    }

    @Test
    public void elProcesoDaDeBajaLosPOIs() {
        BajaDePOIs proceso = new BajaDePOIs(servicioMock, mapa, new DespertadorEstandar(), new Bombero());

        assertEquals(9, mapa.todosLosPOIs().size());

        proceso.ejecutar(new Sindicalista());

        new Demorador().demorar(10);

        assertEquals(7, mapa.todosLosPOIs().size());
    }

    @Test
    public void elProcesoCuentaLosElementosAfectados() {
        BajaDePOIs proceso = new BajaDePOIs(servicioMock, mapa, new DespertadorEstandar(), new Bombero());

        proceso.ejecutar(new Sindicalista());

        new Demorador().demorar(10);

        assertEquals(2, proceso.getElementosAfectados());
    }
}
