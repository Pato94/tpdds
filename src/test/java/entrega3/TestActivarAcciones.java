package entrega3;

import accion.AccionesDesactivadasException;
import accion.BuscarCGPs;
import accion.BuscarEnMapa;
import accion.ConsultarDisponibilidad;
import alarma.ActivadorAcciones;
import busqueda.Buscador;
import busqueda.RepositorioBusquedas;
import dummy.EnviadorDeMailsMock;
import dummy.POIStub;
import dummy.PatoElGranTesteador;
import fixture.FixturePOIS;
import fixture.FixtureUbicacion;
import org.junit.Before;
import org.junit.Test;
import proceso.DespertadorEstandar;
import proceso.Sindicalista;
import proceso.accionesusuarios.ActivarAcciones;
import proceso.accionesusuarios.ListaDeterminada;
import proceso.accionesusuarios.TodosLosUsuarios;
import proceso.accionesusuarios.UsuariosDeUnaComuna;
import proceso.bombero.Bombero;
import ubicacion.Comuna;
import usuario.RepositorioUsuarios;
import usuario.Terminal;
import usuario.Usuario;
import utils.Demorador;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by Pato on 9/16/16.
 */
public class TestActivarAcciones {

    private PatoElGranTesteador usuario;
    private Terminal terminal;
    private POIStub poiStub;
    private List<Usuario> listaUsuarios;
    private RepositorioUsuarios repoUsuarios;
    private Comuna comuna;
    private RepositorioBusquedas repoBusquedas;
    private Buscador buscador;

    @Before
    public void setUp() {
        poiStub = new FixturePOIS().poiStub;
        comuna = new FixtureUbicacion().comuna;

        usuario = new PatoElGranTesteador(null);
        terminal = new Terminal("Una terminal", "laterminal@gmail.com", comuna);

        listaUsuarios = Arrays.asList(usuario, terminal);
        repoUsuarios = new RepositorioUsuarios(listaUsuarios);

        repoBusquedas = new RepositorioBusquedas(new EnviadorDeMailsMock());
        buscador = new Buscador(repoBusquedas, new LinkedList<>());
    }

    @Test
    public void testSePuedenActivarAcciones() {
        usuario.habilitarAccion(ConsultarDisponibilidad.class);

        usuario.realizarAccion(new ConsultarDisponibilidad(poiStub, LocalDateTime.now(), null));

        assertEquals(1, poiStub.estaDisponibleTimesCalled);
    }

    @Test(expected = AccionesDesactivadasException.class)
    public void testSePuedenDeshabilitarAcciones() {
        usuario.habilitarAccion(ConsultarDisponibilidad.class);
        usuario.deshabilitarAccion(ConsultarDisponibilidad.class);

        usuario.realizarAccion(new ConsultarDisponibilidad(poiStub, LocalDateTime.now(), null));
    }

    @Test
    public void sePuedenActivarAccionesParaUnaListaDeUsuariosDeterminados() {
        ActivarAcciones activarAcciones = new ActivarAcciones(
            new ListaDeterminada(listaUsuarios),
            Arrays.asList(ConsultarDisponibilidad.class, BuscarCGPs.class),
            new DespertadorEstandar(),
            new Bombero()
        );

        activarAcciones.ejecutar(new Sindicalista());

        new Demorador().demorar(10);

        usuario.realizarAccion(new ConsultarDisponibilidad(poiStub, LocalDateTime.now(), null));

        assertEquals(1, poiStub.estaDisponibleTimesCalled);

        terminal.realizarAccion(new ConsultarDisponibilidad(poiStub, LocalDateTime.now(), null));

        assertEquals(2, poiStub.estaDisponibleTimesCalled);
    }

    @Test
    public void sePuedenActivarAccionesParaTodosLosUsuariosDelSistema() {
        ActivarAcciones activarAcciones = new ActivarAcciones(
                new TodosLosUsuarios(repoUsuarios),
                Arrays.asList(ConsultarDisponibilidad.class, BuscarCGPs.class),
                new DespertadorEstandar(),
                new Bombero()
        );

        activarAcciones.ejecutar(new Sindicalista());

        new Demorador().demorar(10);

        usuario.realizarAccion(new ConsultarDisponibilidad(poiStub, LocalDateTime.now(), null));

        assertEquals(1, poiStub.estaDisponibleTimesCalled);
    }

    @Test
    public void sePuedenActivarAccionesParaTodosLosUsuariosDeUnaComuna() {
        ActivarAcciones activarAcciones = new ActivarAcciones(
                new UsuariosDeUnaComuna(repoUsuarios, comuna),
                Arrays.asList(ConsultarDisponibilidad.class, BuscarCGPs.class),
                new DespertadorEstandar(),
                new Bombero()
        );

        activarAcciones.ejecutar(new Sindicalista());

        new Demorador().demorar(10);

        boolean exploto = false;

        try {
            usuario.realizarAccion(new ConsultarDisponibilidad(poiStub, LocalDateTime.now(), null));
        } catch (RuntimeException e) {
            exploto = true;
        }

        assertEquals(true, exploto);
        assertEquals(0, poiStub.estaDisponibleTimesCalled);

        terminal.realizarAccion(new ConsultarDisponibilidad(poiStub, LocalDateTime.now(), null));

        assertEquals(1, poiStub.estaDisponibleTimesCalled);
    }

    @Test
    public void sePuedenActivarAccionesDespuesDeUnaBusqueda() {
        repoBusquedas.agregarAlarma(
                new ActivadorAcciones(new Sindicalista(),
                        Collections.singletonList(ConsultarDisponibilidad.class)));

        usuario.habilitarAccion(BuscarEnMapa.class);
        assertEquals(1, usuario.getAccionesPermitidas().size());

        usuario.realizarAccion(new BuscarEnMapa(buscador, "pato"));

        new Demorador().demorar(10);

        assertEquals(2, usuario.getAccionesPermitidas().size());
    }

}
