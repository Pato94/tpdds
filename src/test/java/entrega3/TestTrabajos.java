package entrega3;

import dummy.ServicioMailingMock;
import dummy.TrabajoQueDemora;
import dummy.TrabajoQueFallaNVeces;
import dummy.TrabajoStub;
import org.junit.Before;
import org.junit.Test;
import proceso.Sindicalista;
import proceso.bombero.EnviarUnMail;
import proceso.bombero.ReintentarNVeces;
import utils.Demorador;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import static org.junit.Assert.*;

public class TestTrabajos {

    private Sindicalista sindicalista;
    private Demorador demorador;

    @Before
    public void setUp() {
        sindicalista = new Sindicalista();
        demorador = new Demorador();
    }

    @Test
    public void testSePuedeEjecutarUnTrabajoEnElMomento() {
        TrabajoStub trabajoBobo = new TrabajoStub();

        trabajoBobo.ejecutar(sindicalista);

        demorador.demorar(5);

        assertTrue(trabajoBobo.seRealizo());
    }

    @Test
    public void testSePuedeProgramarUnTrabajoParaUnMomentoFuturo() {
        TrabajoStub trabajoBobo = new TrabajoStub();

        trabajoBobo.ejecutarEn(LocalDateTime.now().plus(90, ChronoUnit.MILLIS), sindicalista);

        assertFalse(trabajoBobo.seRealizo());

        demorador.demorar(100);

        assertTrue(trabajoBobo.seRealizo());
    }

    @Test
    public void testElSindicalistaDemoraTrabajosSiEstaOcupado() {
        TrabajoStub trabajoBobo1 = new TrabajoStub();
        TrabajoQueDemora trabajoBobo2 = new TrabajoQueDemora(100);

        LocalDateTime dentroDe70Millis = LocalDateTime.now().plus(70, ChronoUnit.MILLIS);

        trabajoBobo1.ejecutarEn(dentroDe70Millis, sindicalista);
        trabajoBobo2.ejecutarAhora(sindicalista);

        demorador.demorar(90);

        assertFalse(trabajoBobo1.seRealizo());

        demorador.demorar(100);

        assertTrue(trabajoBobo1.seRealizo());
    }

    @Test
    public void testReintentarNVecesFunciona() {
        ReintentarNVeces bombero = new ReintentarNVeces(3);
        TrabajoQueFallaNVeces trabajoBobo = new TrabajoQueFallaNVeces(5, bombero);

        trabajoBobo.ejecutarAhora(sindicalista);

        //Es mas importante de lo que pensas
        demorador.demorar(10);

        assertEquals(3, bombero.getVecesIntentadas());
        assertEquals(4, trabajoBobo.getVecesQueFallo());
    }

    @Test
    public void testEnviarMailSiFalla() {
        ServicioMailingMock servicioDeMailingMock = new ServicioMailingMock();
        EnviarUnMail bombero = new EnviarUnMail(servicioDeMailingMock);
        TrabajoQueFallaNVeces trabajoBobo = new TrabajoQueFallaNVeces(2, bombero);

        assertEquals(0, servicioDeMailingMock.getMailsEnviados().size());

        trabajoBobo.ejecutarAhora(sindicalista);
        demorador.demorar(10);

        assertEquals(1, servicioDeMailingMock.getMailsEnviados().size());

        trabajoBobo.ejecutarAhora(sindicalista);
        demorador.demorar(10);

        assertEquals(2, servicioDeMailingMock.getMailsEnviados().size());
    }
}