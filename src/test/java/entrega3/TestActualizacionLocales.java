package entrega3;

import dummy.FuenteDeInfoStub;
import fixture.FixturePOIS;
import org.junit.Before;
import org.junit.Test;
import origen.Mapa;
import proceso.Sindicalista;
import proceso.actualizacionlocales.ActualizacionLocales;
import proceso.actualizacionlocales.InfoLocal;
import utils.Demorador;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

/**
 * Created by Pato on 9/15/16.
 */
public class TestActualizacionLocales {
    private Mapa mapa;
    private FuenteDeInfoStub fuente;

    @Before
    public void setUp() {
        this.mapa = new FixturePOIS().mapaConTodosLosPOIs;
        this.fuente = new FuenteDeInfoStub();
    }

    @Test
    public void sePuedenRecuperarDatosDeElFormatoIndicado1() {
        String testString1 = "PatoPCs;computacion servicio_tecnico humo";
        InfoLocal infoLocal = InfoLocal.fromString(testString1);

        assertEquals("PatoPCs", infoLocal.getNombre());
        assertArrayEquals(new String[]{"computacion", "servicio_tecnico", "humo"}, infoLocal.getTags().toArray());
    }

    @Test
    public void seToleraInfoSinTags() {
        String testString2 = "PatoPCs;";
        InfoLocal infoLocal = InfoLocal.fromString(testString2);

        assertEquals("PatoPCs", infoLocal.getNombre());
        assertArrayEquals(new String[]{}, infoLocal.getTags().toArray());

        String testString3 = "PatoPCs";
        InfoLocal infoLocal1 = InfoLocal.fromString(testString3);

        assertEquals("PatoPCs", infoLocal1.getNombre());
        assertArrayEquals(new String[]{}, infoLocal.getTags().toArray());
    }

    @Test
    public void testElProcesoCuentaLaCantidadDeResultados() {
        ActualizacionLocales actualizacionLocales = new ActualizacionLocales(null, null, fuente, mapa);

        fuente.agregarDato("Muebles Locos");
        fuente.agregarDato("Carpinteria El tano");
        fuente.agregarDato("Complot"); //Este local es inventado, no deberia contar
        fuente.agregarDato("Lo de Carlitos");

        actualizacionLocales.ejecutar(new Sindicalista());

        new Demorador().demorar(100);

        assertEquals(3, actualizacionLocales.getElementosAfectados());
    }

    @Test
    public void testElProcesoActualizaLosTags() {
        ActualizacionLocales actualizacionLocales = new ActualizacionLocales(null, null, fuente, mapa);

        assertArrayEquals(new String[]{"groso", "bueno", "buenisimo"}, ActualizacionLocales.buscarLocalPorNombre(mapa, "Muebles Locos").getTags().toArray());

        fuente.agregarDato("Muebles Locos;muebles");

        actualizacionLocales.ejecutar(new Sindicalista());

        new Demorador().demorar(100);

        assertEquals(1, actualizacionLocales.getElementosAfectados());
        assertArrayEquals(new String[]{"muebles"}, ActualizacionLocales.buscarLocalPorNombre(mapa, "Muebles Locos").getTags().toArray());
    }
}
