package entrega1;

import consulta.ConsultaCercania;
import fixture.FixtureUbicacion;
import org.junit.Before;
import org.junit.Test;
import poi.*;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestCercania {

    private FixtureUbicacion fixtureUbicacion;
    private ConsultaCercania consultaCercaniaEnUbicacion;
    private ConsultaCercania consultaCercaniaAMenosDeUnaCuadra;
    private ConsultaCercania consultaCercaniaAMasDeUnaCuadra;

    @Before
    public void setUp() {
        fixtureUbicacion = new FixtureUbicacion();
        consultaCercaniaEnUbicacion = new ConsultaCercania(fixtureUbicacion.ubicacion, fixtureUbicacion.comuna);
        consultaCercaniaAMenosDeUnaCuadra = new ConsultaCercania(fixtureUbicacion.ubicacionAMenosDeUnaCuadra, fixtureUbicacion.comuna);
        consultaCercaniaAMasDeUnaCuadra = new ConsultaCercania(fixtureUbicacion.ubicacionAMasDeUnaCuadra, fixtureUbicacion.comuna);
    }

    @Test
    public void testBancoEsCercanoSiEstaAMenosDe5Cuadras() {
        Banco banco = new Banco(1, fixtureUbicacion.ubicacionAMenosDe5Cuadras);

        assertTrue(banco.esCercano(consultaCercaniaEnUbicacion));
    }

    @Test
    public void testBancoNoEsCercanoSiEstaAMasDe5Cuadras() {
        Banco banco = new Banco(1, fixtureUbicacion.ubicacionAMasDe5Cuadras);

        assertFalse(banco.esCercano(consultaCercaniaEnUbicacion));
    }

    @Test
    public void paradaDeColectivoEsCercanaSiEstaAMenosDeUnaCuadra() {
        POI parada = new Parada(1, fixtureUbicacion.ubicacionAMenosDeUnaCuadra, null);

        assertTrue(parada.esCercano(consultaCercaniaEnUbicacion));
    }

    @Test
    public void paradaDeColectivosNoEsCercanaSiEstaAMasDeUnaCuadra() {
        POI parada = new Parada(1, fixtureUbicacion.ubicacionAMasDeUnaCuadra, null);

        assertFalse(parada.esCercano(consultaCercaniaEnUbicacion));
    }

    @Test
    public void gcpEsCercanoSiPerteneceALaComuna() {
        CGP cgp = new CGP(1, fixtureUbicacion.ubicacionDentroDeLaComuna, null);

        assertTrue(cgp.esCercano(consultaCercaniaEnUbicacion));
    }

    @Test
    public void gcpNoEsCercanoSiNoPerteneceALaComuna() {
        CGP cgp = new CGP(1, fixtureUbicacion.ubicacionFueraDeLaComuna, null);

        assertFalse(cgp.esCercano(consultaCercaniaEnUbicacion));
    }

    @Test
    public void localEsCercanoEnFuncionDelRubro() {
        Rubro rubro = new Rubro("testing", 100);
        Local local = new Local(1, "A local has no name", fixtureUbicacion.ubicacion, rubro, null);

        assertTrue(local.esCercano(consultaCercaniaAMenosDeUnaCuadra));
        assertFalse(local.esCercano(consultaCercaniaAMasDeUnaCuadra));
    }
}
