package entrega1;

import fixture.FixturePOIS;
import org.junit.Before;
import org.junit.Test;
import origen.Mapa;
import poi.POI;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestBusqueda {

    private Mapa mapa;
    private FixturePOIS fixturePOIS;

    @Before
    public void setUp() {
        this.fixturePOIS = new FixturePOIS();
        setUpMapa();
    }

    private void setUpMapa() {
        this.mapa = new Mapa(Arrays.asList(
                fixturePOIS.bancoConAtencionAlCliente,
                fixturePOIS.bancoConTodosLosDepositos,
                fixturePOIS.gcpConLimpieza,
                fixturePOIS.gcpConLimpiezaYDeposito,
                fixturePOIS.paradaDel114,
                fixturePOIS.localDeMadera,
                fixturePOIS.localDeMuebles,
                fixturePOIS.localDePanchos,
                fixturePOIS.localDePanchosDeCarlitos
        ));
    }

    @Test
    public void testParadaConsideraLineasQuePasan() {
        List<POI> resultado = mapa.buscarPois("114");

        assertTrue(resultado.contains(fixturePOIS.paradaDel114));
        assertEquals(1, resultado.size());
    }

    @Test
    public void testLocalConsideranRubro() {
        List<POI> resultado = mapa.buscarPois("pancheria");

        assertTrue(resultado.contains(fixturePOIS.localDePanchos));
        assertTrue(resultado.contains(fixturePOIS.localDePanchosDeCarlitos));
        assertEquals(2, resultado.size());
    }

    @Test
    public void testCGPConsideraServicios() {
        List<POI> resultado = mapa.buscarPois("limpi");

        assertTrue(resultado.contains(fixturePOIS.gcpConLimpieza));
        assertTrue(resultado.contains(fixturePOIS.gcpConLimpiezaYDeposito));
        assertEquals(2, resultado.size());
    }

    @Test
    public void testPuedoBuscarPOISTageados() {
        List<POI> resultado = mapa.buscarPois("BUENO");

        assertTrue(resultado.contains(fixturePOIS.bancoConAtencionAlCliente));
        assertTrue(resultado.contains(fixturePOIS.paradaDel114));
        assertTrue(resultado.contains(fixturePOIS.localDeMuebles));
        assertEquals(3, resultado.size());
    }
}
