package entrega1;

import consulta.ConsultaDisponibilidad;
import fixture.FixtureHorarios;
import fixture.FixtureUbicacion;
import horarios.DisponibilidadDiasHabiles;
import horarios.DisponibilidadHoraria;
import horarios.DisponibilidadParaDia;
import org.junit.Before;
import org.junit.Test;
import poi.*;

import java.time.LocalDateTime;
import java.util.Arrays;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestDisponibilidad {

    private FixtureUbicacion fixtureUbicacion;
    private FixtureHorarios fixtureHorarios;

    @Before
    public void setUp() {
        this.fixtureUbicacion = new FixtureUbicacion();
        this.fixtureHorarios = new FixtureHorarios();
    }

    @Test
    public void testParadaDisponibleEnTodoMomento() {
        POI parada = new Parada(1, fixtureUbicacion.ubicacion, null);
        ConsultaDisponibilidad consultaDisponibilidad = new ConsultaDisponibilidad(LocalDateTime.now());

        assertTrue(parada.estaDisponible(consultaDisponibilidad));
    }

    @Test
    public void testCGPSiSeIngresaUnValorX() {
        DisponibilidadDiasHabiles todosLosDiasDe9A18 = fixtureHorarios.disponibilidadTodosLosDiasDe9A18;
        Servicio atencionAlPublico = new Servicio("Atencion al publico", todosLosDiasDe9A18);

        DisponibilidadParaDia lunesDe9A18 = fixtureHorarios.disponibilidadParaLunesDe9A18;
        Servicio limpieza = new Servicio("Limpieza", lunesDe9A18);

        CGP cgp = new CGP(1, fixtureUbicacion.ubicacion, Arrays.asList(atencionAlPublico, limpieza));

        assertTrue(cgp.estaDisponible(new ConsultaDisponibilidad(fixtureHorarios.lunes0901, "Limpieza")));
        assertFalse(cgp.estaDisponible(new ConsultaDisponibilidad(fixtureHorarios.martes0901, "Limpieza")));
    }

    @Test
    public void testCGPSiNoSeIngresaUnValorX() {
        DisponibilidadDiasHabiles todosLosDiasDe9A18 = fixtureHorarios.disponibilidadTodosLosDiasDe9A18;
        Servicio atencionAlPublico = new Servicio("Atencion al publico", todosLosDiasDe9A18);

        DisponibilidadParaDia lunesDe9A18 = fixtureHorarios.disponibilidadParaLunesDe9A18;
        Servicio limpieza = new Servicio("Limpieza", lunesDe9A18);

        CGP cgp = new CGP(1, fixtureUbicacion.ubicacion, Arrays.asList(atencionAlPublico, limpieza));

        assertTrue(cgp.estaDisponible(new ConsultaDisponibilidad(fixtureHorarios.lunes0901)));
        assertTrue(cgp.estaDisponible(new ConsultaDisponibilidad(fixtureHorarios.martes0901)));
    }

    @Test
    public void bancoConsideraElNombreDelServicio() {
        DisponibilidadHoraria lunesDe10A11 = fixtureHorarios.disponibilidadParaLunesDe10A11;
        Servicio atencionAJubilados = new Servicio("Atencion a jubilados", lunesDe10A11); //ja, ja

        Banco banco = new Banco(1, fixtureUbicacion.ubicacion, Arrays.asList(atencionAJubilados));

        assertFalse(banco.estaDisponible(new ConsultaDisponibilidad(fixtureHorarios.lunes0901, "Atencion a jubilados")));
        assertTrue(banco.estaDisponible(new ConsultaDisponibilidad(fixtureHorarios.lunes1001, "Atencion a jubilados")));
    }

    @Test
    public void bancoUsaPorDefectoElHorarioBancario() {
        DisponibilidadHoraria lunesDe10A11 = fixtureHorarios.disponibilidadParaLunesDe10A11;
        Servicio atencionAJubilados = new Servicio("Atencion a jubilados", lunesDe10A11); //ja, ja

        Banco banco = new Banco(1, fixtureUbicacion.ubicacion, Arrays.asList(atencionAJubilados));

        assertTrue(banco.estaDisponible(new ConsultaDisponibilidad(fixtureHorarios.martes1001)));
        assertTrue(banco.estaDisponible(new ConsultaDisponibilidad(fixtureHorarios.lunes1001)));
    }

    @Test
    public void localUsaElHorarioDelLocal() {
        Local local = new Local(1, "A local has no name", fixtureUbicacion.ubicacion, null,
                fixtureHorarios.disponibilidadParaLunesYMiercolesDe9A10);

        assertTrue(local.estaDisponible(new ConsultaDisponibilidad(fixtureHorarios.lunes0901)));
        assertFalse(local.estaDisponible(new ConsultaDisponibilidad(fixtureHorarios.martes0901)));
        assertTrue(local.estaDisponible(new ConsultaDisponibilidad(fixtureHorarios.miercoles0901)));
    }
}
