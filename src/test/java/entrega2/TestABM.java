package entrega2;

import fixture.FixturePOIS;
import org.junit.Before;
import org.junit.Test;
import origen.Mapa;

import java.util.LinkedList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestABM {

    private Mapa mapa;
    private FixturePOIS fixturePOIS;

    @Before
    public void setUp() {
        mapa = new Mapa(new LinkedList<>());
        fixturePOIS = new FixturePOIS();
    }

    @Test
    public void testUnAdminPuedeAgregarPuntosAlSistema() {
        mapa.agregarPOI(fixturePOIS.gcpConLimpieza);

        assertEquals(1, mapa.todosLosPOIs().size());
        assertTrue(mapa.todosLosPOIs().contains(fixturePOIS.gcpConLimpieza));
    }

    @Test
    public void testUnAdminPuedeBorrarPuntosDelSistema() {
        mapa.agregarPOI(fixturePOIS.gcpConLimpieza);

        assertEquals(1, mapa.todosLosPOIs().size());

        mapa.removerPOI(fixturePOIS.gcpConLimpieza);

        assertEquals(0, mapa.todosLosPOIs().size());
    }

    @Test
    public void testUnAdminPuedeModificarPuntosDelSistema() {
        //? Preguntar
    }
}
