package entrega2;

import busqueda.Buscador;
import busqueda.BusquedaCGPs;
import busqueda.RepositorioBusquedas;
import dummy.*;
import dummy.ServicioMailingMock.MailData;
import org.junit.Before;
import org.junit.Test;
import origen.WrapperComponenteCGPs;

import java.util.Collections;
import java.util.LinkedList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestRepoBusquedas {

    private RepositorioBusquedas repoBusquedas;
    private Buscador buscador;
    private EnviadorDeMailsMock enviadorDeMails;
    private LinkedList<MailData> mailsEnviados;

    @Before
    public void setUp() {
        enviadorDeMails = new EnviadorDeMailsMock();
        mailsEnviados = enviadorDeMails.getServicioMailing().getMailsEnviados();
        repoBusquedas = new RepositorioBusquedas(enviadorDeMails);
        buscador = new Buscador(repoBusquedas,
                Collections.singletonList(new WrapperComponenteCGPs(new ComponenteCGPsMock())));
    }

    @Test
    public void testLasBusquedasRealizadasSeAgreganAlRepo() {
        assertEquals(0, repoBusquedas.getBusquedas().size());
        BusquedaQueNadieJuna busqueda = new BusquedaQueNadieJuna();
        buscador.buscar(busqueda);

        assertEquals(1, repoBusquedas.getBusquedas().size());
        assertTrue(repoBusquedas.getBusquedas().contains(busqueda));
    }

    @Test
    public void testLasBusquedasAlmacenanLaCantidadDeResultados() {
        BusquedaQueNadieJuna busquedaQueNadieJuna = new BusquedaQueNadieJuna();
        BusquedaCGPs busquedaCGPs = new BusquedaCGPs(new PatoElGranTesteador(null), "");

        buscador.buscar(busquedaQueNadieJuna);
        buscador.buscar(busquedaCGPs);

        assertEquals(0, repoBusquedas.getBusquedas().get(0).getCantidadDeResultados());
        assertEquals(3, repoBusquedas.getBusquedas().get(1).getCantidadDeResultados());
    }

    @Test
    public void testLasBusquedasAlmacenanLaDuracionDeLaBusqueda() {
        BusquedaQueAgregaDelay busquedaQueTrabaTodo100ms = new BusquedaQueAgregaDelay(100);

        buscador.buscar(busquedaQueTrabaTodo100ms);

        assertTrue(100 <= repoBusquedas.getBusquedas().get(0).getDuracionEnMillis());
    }

    @Test
    public void testElRepoAvisaAlAdminSiLaBusquedaTardaMasDeXTiempo() {
        //El mock seteado tiene una tolerancia de 150 ms
        BusquedaQueAgregaDelay busquedaQueTrabaTodo200ms = new BusquedaQueAgregaDelay(200);

        assertEquals(0, mailsEnviados.size());

        buscador.buscar(busquedaQueTrabaTodo200ms);

        assertEquals(1, mailsEnviados.size());
        assertEquals("pato@eltesterloco.com", mailsEnviados.get(0).getEmail());
    }

}
