package entrega2;

import busqueda.RepositorioBusquedas;
import dummy.BusquedaConFecha;
import dummy.EnviadorDeMailsMock;
import fixture.FixtureBusquedas;
import informe.*;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class TestInforme {

    private RepositorioBusquedas repoBusquedas;
    private FixtureBusquedas fixtureBusquedas;

    @Before
    public void setUp() {
        fixtureBusquedas = new FixtureBusquedas();
        repoBusquedas = new RepositorioBusquedas(new EnviadorDeMailsMock());
    }

    @Test
    public void testSePuedenAgruparBusquedasPorFecha() {
        InformeBusqueda informeAgrupadoPorFecha = new AgrupadoPorFecha(new InformeBusquedaBase());
        repoBusquedas.agregarBusqueda(fixtureBusquedas.busqueda070716_0707);
        repoBusquedas.agregarBusqueda(fixtureBusquedas.busqueda070716_0807);
        repoBusquedas.agregarBusqueda(fixtureBusquedas.busqueda090716_0707);

        List<ItemInformeBusqueda> informe = informeAgrupadoPorFecha.generarInforme(repoBusquedas.getBusquedas());

        assertEquals(2, informe.size());

        assertEquals("07/07/2016", informe.get(0).getGrupo());
        assertEquals(2, informe.get(0).getCantidad());

        assertEquals("09/07/2016", informe.get(1).getGrupo());
        assertEquals(1, informe.get(1).getCantidad());
    }

    @Test
    public void testSePuedenAgruparBusquedasPorClase() {
        InformeBusqueda informeAgrupadoPorClase = new AgrupadoPorClase(new InformeBusquedaBase());
        repoBusquedas.agregarBusqueda(fixtureBusquedas.busqueda070716_0707);
        repoBusquedas.agregarBusqueda(fixtureBusquedas.busqueda080716_0707);
        repoBusquedas.agregarBusqueda(fixtureBusquedas.busquedaBancos);
        repoBusquedas.agregarBusqueda(fixtureBusquedas.busquedaCGPs);
        repoBusquedas.agregarBusqueda(fixtureBusquedas.busquedaMapa);
        repoBusquedas.agregarBusqueda(fixtureBusquedas.busqueda090716_0807);

        List<ItemInformeBusqueda> informe = informeAgrupadoPorClase.generarInforme(repoBusquedas.getBusquedas());

        assertEquals(4, informe.size());

        assertEquals("BusquedaCGPs", informe.get(0).getGrupo());
        assertEquals(1, informe.get(0).getCantidad());

        assertEquals("BusquedaBancos", informe.get(1).getGrupo());
        assertEquals(1, informe.get(1).getCantidad());

        assertEquals("BusquedaConFecha", informe.get(2).getGrupo());
        assertEquals(3, informe.get(2).getCantidad());

        assertEquals("BusquedaEnMapa", informe.get(3).getGrupo());
        assertEquals(1, informe.get(3).getCantidad());
    }

    @Test
    public void testSePuedenFiltrarBusquedasPorClase() {
        InformeBusqueda informeFiltradoPorClase =
                new FiltradoPorClase(new InformeBusquedaBase(), BusquedaConFecha.class);

        repoBusquedas.agregarBusqueda(fixtureBusquedas.busqueda070716_0707);
        repoBusquedas.agregarBusqueda(fixtureBusquedas.busqueda080716_0707);
        repoBusquedas.agregarBusqueda(fixtureBusquedas.busquedaBancos);
        repoBusquedas.agregarBusqueda(fixtureBusquedas.busquedaCGPs);
        repoBusquedas.agregarBusqueda(fixtureBusquedas.busquedaMapa);
        repoBusquedas.agregarBusqueda(fixtureBusquedas.busqueda090716_0807);

        List<ItemInformeBusqueda> informe = informeFiltradoPorClase.generarInforme(repoBusquedas.getBusquedas());

        assertEquals(1, informe.size());

        assertEquals("", informe.get(0).getGrupo());
        assertEquals(3, informe.get(0).getCantidad());
    }

    @Test
    public void testLosDecoradoresSonCombinables() {
        InformeBusqueda informeFiltradoPorClaseYAgrupadoPorFecha =
                new FiltradoPorClase(
                        new AgrupadoPorFecha(new InformeBusquedaBase()), BusquedaConFecha.class);

        repoBusquedas.agregarBusqueda(fixtureBusquedas.busqueda070716_0707);
        repoBusquedas.agregarBusqueda(fixtureBusquedas.busqueda080716_0707);
        repoBusquedas.agregarBusqueda(fixtureBusquedas.busquedaBancos);
        repoBusquedas.agregarBusqueda(fixtureBusquedas.busquedaCGPs);
        repoBusquedas.agregarBusqueda(fixtureBusquedas.busquedaMapa);
        repoBusquedas.agregarBusqueda(fixtureBusquedas.busqueda090716_0807);

        List<ItemInformeBusqueda> informe =
                informeFiltradoPorClaseYAgrupadoPorFecha.generarInforme(repoBusquedas.getBusquedas());

        assertEquals(3, informe.size());

        assertEquals("07/07/2016", informe.get(0).getGrupo());
        assertEquals(1, informe.get(0).getCantidad());

        assertEquals("09/07/2016", informe.get(1).getGrupo());
        assertEquals(1, informe.get(1).getCantidad());

        assertEquals("08/07/2016", informe.get(2).getGrupo());
        assertEquals(1, informe.get(2).getCantidad());
    }

    @Test
    public void testAgruparPorNombreUsuario() {
        InformeBusqueda informeAgrupadoPorNombreUsuario =
                new AgrupadoPorNombreUsuario(new InformeBusquedaBase());

        repoBusquedas.agregarBusqueda(fixtureBusquedas.busqueda070716_0707);
        repoBusquedas.agregarBusqueda(fixtureBusquedas.busqueda080716_0707);
        repoBusquedas.agregarBusqueda(fixtureBusquedas.busquedaBancos);
        repoBusquedas.agregarBusqueda(fixtureBusquedas.busquedaCGPs);
        repoBusquedas.agregarBusqueda(fixtureBusquedas.busquedaMapa);
        repoBusquedas.agregarBusqueda(fixtureBusquedas.busqueda090716_0807);
        repoBusquedas.agregarBusqueda(fixtureBusquedas.busquedaQueBlockea300ms);

        List<ItemInformeBusqueda> informe =
                informeAgrupadoPorNombreUsuario.generarInforme(repoBusquedas.getBusquedas());

        assertEquals(2, informe.size());

        assertEquals("pato", informe.get(0).getGrupo());
        assertEquals(3, informe.get(0).getCantidad());

        assertEquals("topa", informe.get(1).getGrupo());
        assertEquals(4, informe.get(1).getCantidad());
    }

    @Test
    public void testFiltrarPorNombreUsuario() {
        InformeBusqueda informeFiltradoPorNombreUsuarioFiltradoPorClaseYAgrupadoPorFecha =
                new FiltradoPorNombreUsuario(
                        new FiltradoPorClase(
                                new AgrupadoPorFecha(new InformeBusquedaBase()), BusquedaConFecha.class), "pato");

        repoBusquedas.agregarBusqueda(fixtureBusquedas.busqueda070716_0707);
        repoBusquedas.agregarBusqueda(fixtureBusquedas.busqueda080716_0707);
        repoBusquedas.agregarBusqueda(fixtureBusquedas.busquedaBancos);
        repoBusquedas.agregarBusqueda(fixtureBusquedas.busquedaCGPs);
        repoBusquedas.agregarBusqueda(fixtureBusquedas.busquedaMapa);
        repoBusquedas.agregarBusqueda(fixtureBusquedas.busqueda090716_0807);
        repoBusquedas.agregarBusqueda(fixtureBusquedas.busquedaQueBlockea300ms);

        List<ItemInformeBusqueda> informe =
                informeFiltradoPorNombreUsuarioFiltradoPorClaseYAgrupadoPorFecha.generarInforme(repoBusquedas.getBusquedas());

        assertEquals(3, informe.size());

        assertEquals("07/07/2016", informe.get(0).getGrupo());
        assertEquals(1, informe.get(0).getCantidad());

        assertEquals("09/07/2016", informe.get(1).getGrupo());
        assertEquals(1, informe.get(1).getCantidad());

        assertEquals("08/07/2016", informe.get(2).getGrupo());
        assertEquals(1, informe.get(2).getCantidad());
    }
}
