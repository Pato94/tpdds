package entrega2;

import external.CentroDTO;
import external.ServicioDTO;
import fixture.FixtureHorarios;
import org.junit.Before;
import org.junit.Test;
import poi.CGP;
import poi.Servicio;

import java.util.Arrays;

import static org.junit.Assert.*;

public class TestCGPExterno {

    private FixtureHorarios fixtureHorarios;

    @Before
    public void setUp() {
        fixtureHorarios = new FixtureHorarios();
    }

    @Test
    public void testConversionServicioDTOSeRealizaCorrectamente() {
        ServicioDTO servicioDTO = new ServicioDTO("Test", new int[]{1, 2, 3}, 9, 0, 18, 0);
        Servicio servicio = Servicio.fromServicioDTO(servicioDTO);

        assertEquals("Test", servicio.getNombre());
        assertTrue(servicio.estaDisponible(fixtureHorarios.lunes0901));
        assertTrue(servicio.estaDisponible(fixtureHorarios.martes0901));
        assertTrue(servicio.estaDisponible(fixtureHorarios.miercoles0901));
        assertFalse(servicio.estaDisponible(fixtureHorarios.jueves0901));
    }

    @Test
    public void testConversionCGPSeRealizaCorrectamente() {
        ServicioDTO servicioDTO1 = new ServicioDTO("Test 1", new int[]{1, 2, 3, 4, 5}, 9, 0, 18, 0);
        ServicioDTO servicioDTO2 = new ServicioDTO("Test 2", new int[]{1, 2, 3}, 9, 0, 18, 0);
        ServicioDTO servicioDTO3 = new ServicioDTO("Test 3", new int[]{3}, 9, 0, 11, 0);
        CentroDTO centroDTO = new CentroDTO(1, null, null, null, null, Arrays.asList(servicioDTO1, servicioDTO2, servicioDTO3));

        CGP cgp = CGP.fromCentroDTO(centroDTO);

        assertEquals(3, cgp.getServicios().size());
        assertEquals("Test 1", cgp.getServicios().get(0).getNombre());
    }
}
