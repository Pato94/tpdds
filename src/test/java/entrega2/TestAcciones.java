package entrega2;

import accion.*;
import busqueda.Buscador;
import busqueda.RepositorioBusquedas;
import dummy.ComponenteCGPsMock;
import dummy.EnviadorDeMailsMock;
import dummy.ServicioRestBancosMock;
import fixture.FixturePOIS;
import fixture.FixtureUbicacion;
import org.junit.Before;
import org.junit.Test;
import origen.WrapperComponenteCGPs;
import origen.WrapperServicioBancos;
import usuario.Terminal;

import java.time.LocalDateTime;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class TestAcciones {

    private Terminal terminal;
    private Buscador buscador;
    private RepositorioBusquedas repo;
    private FixturePOIS fixturePOIs;
    private FixtureUbicacion fixtureUbicacion;

    @Before
    public void setUp() {
        Terminal.setAccionesHabilitadas(true);
        terminal = new Terminal("terminal", "terminal@loca.com", null);
        terminal.habilitarAcciones(Arrays.asList(BuscarBancos.class, BuscarCGPs.class,
                BuscarEnMapa.class, ConsultarDisponibilidad.class, ConsultarCercania.class));
        repo = new RepositorioBusquedas(new EnviadorDeMailsMock());
        buscador = new Buscador(
                repo,
                Arrays.asList(
                        new WrapperComponenteCGPs(new ComponenteCGPsMock()),
                        new WrapperServicioBancos(new ServicioRestBancosMock())
                )
        );
        fixturePOIs = new FixturePOIS();
        fixtureUbicacion = new FixtureUbicacion();
    }

    @Test
    public void testUnUsuarioPuedeBuscarBancos() {
        AccionSistema buscarBancos = new BuscarBancos(buscador, "", null);

        assertEquals(0, repo.getBusquedas().size());

        terminal.realizarAccion(buscarBancos);

        assertEquals(1, repo.getBusquedas().size());
    }

    @Test
    public void testUnUsuarioPuedeBuscarCGPs() {
        AccionSistema buscarCGPs = new BuscarCGPs(buscador, "");

        assertEquals(0, repo.getBusquedas().size());

        terminal.realizarAccion(buscarCGPs);

        assertEquals(1, repo.getBusquedas().size());
    }

    @Test
    public void testUnUsuarioPuedeBuscarEnElMapa() {
        AccionSistema buscarEnMapa = new BuscarEnMapa(buscador, "");

        assertEquals(0, repo.getBusquedas().size());

        terminal.realizarAccion(buscarEnMapa);

        assertEquals(1, repo.getBusquedas().size());
    }

    @Test
    public void testUnUsuarioPuedeConsultarDisponibilidad() {
        AccionSistema consultarDisponibilidadParada =
                new ConsultarDisponibilidad(fixturePOIs.poiStub, LocalDateTime.now(), null);

        assertEquals(0, fixturePOIs.poiStub.estaDisponibleTimesCalled);

        terminal.realizarAccion(consultarDisponibilidadParada);
        terminal.realizarAccion(consultarDisponibilidadParada);

        assertEquals(2, fixturePOIs.poiStub.estaDisponibleTimesCalled);
    }

    @Test
    public void testUnUsuarioPuedeConsultarCercania() {
        AccionSistema consultarCercaniaParada =
                new ConsultarCercania(fixturePOIs.poiStub, fixtureUbicacion.ubicacion, fixtureUbicacion.comuna);

        assertEquals(0, fixturePOIs.poiStub.esCercanoTimesCalled);

        terminal.realizarAccion(consultarCercaniaParada);

        assertEquals(1, fixturePOIs.poiStub.esCercanoTimesCalled);
    }

    @Test(expected = AccionesDesactivadasException.class)
    public void testSePuedenDesactivarLasAccionesPorTerminal() {
        AccionSistema consultarCercaniaParada =
                new ConsultarCercania(fixturePOIs.poiStub, fixtureUbicacion.ubicacion, fixtureUbicacion.comuna);

        Terminal.setAccionesHabilitadas(false);

        terminal.realizarAccion(consultarCercaniaParada);
    }
}

