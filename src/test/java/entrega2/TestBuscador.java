package entrega2;

import busqueda.*;
import dummy.*;
import fixture.FixturePOIS;
import org.junit.Before;
import org.junit.Test;
import origen.WrapperComponenteCGPs;
import origen.WrapperServicioBancos;
import poi.POI;
import usuario.Usuario;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class TestBuscador {

    private Buscador buscador;
    private FixturePOIS fixturePOIs;
    private Usuario pato;

    @Before
    public void setUp() {
        pato = new PatoElGranTesteador(null);
        fixturePOIs = new FixturePOIS();
        buscador = new Buscador(
                new RepositorioBusquedas(new EnviadorDeMailsMock()),
                Arrays.asList(
                        fixturePOIs.mapaConTodosLosPOIs,
                        new WrapperComponenteCGPs(new ComponenteCGPsMock()),
                        new WrapperServicioBancos(new ServicioRestBancosMock())
                )
        );
    }

    @Test
    public void testPuedoBuscarEnElMapa() {
        BusquedaEnMapa busqueda = new BusquedaEnMapa(pato, "114");
        List<POI> resultado = buscador.buscar(busqueda);

        assertEquals(1, resultado.size());
    }

    @Test
    public void testPuedoBuscarCGPsAlComponenteLoco() {
        BusquedaCGPs busqueda = new BusquedaCGPs(pato, "");
        List<POI> resultado = this.buscador.buscar(busqueda);

        assertEquals(3, resultado.size());
    }

    @Test
    public void testPuedoBuscarBancosConElServicio() {
        BusquedaBancos busqueda = new BusquedaBancos(pato, "", "");
        List<POI> resultado = buscador.buscar(busqueda);

        assertEquals(2, resultado.size());
    }

    @Test
    public void testSiElBuscadorNoTieneUnOrigenQuePuedaResolverEsaBusquedaNoRetornaResultados() {
        Busqueda busqueda = new BusquedaQueNadieJuna();

        assertEquals(0, buscador.buscar(busqueda).size());
    }
}
