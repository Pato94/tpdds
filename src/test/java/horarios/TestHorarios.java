package horarios;

import fixture.FixtureHorarios;
import org.junit.Before;
import org.junit.Test;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.Arrays;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestHorarios {

    private FixtureHorarios fixtureHorarios;

    @Before
    public void setUp() {
        this.fixtureHorarios = new FixtureHorarios();
    }

    @Test
    public void rangoHorarioAceptaHoras() {
        RangoHorario rango = new RangoHorario(LocalTime.of(9, 0), LocalTime.of(10, 0));

        assertTrue(rango.comprende(LocalTime.of(9, 1)));

        assertFalse(rango.comprende(LocalTime.of(8, 1)));
    }

    @Test
    public void disponibilidadParaDiasFunciona() {
        DisponibilidadHoraria disponibilidadParaLunes = new DisponibilidadParaDia(DayOfWeek.MONDAY, LocalTime.of(9, 0),
                LocalTime.of(10, 0));

        assertTrue(disponibilidadParaLunes.disponible(fixtureHorarios.lunes0901));
        assertFalse(disponibilidadParaLunes.disponible(fixtureHorarios.lunes1001));
        assertFalse(disponibilidadParaLunes.disponible(fixtureHorarios.martes0901));
    }

    @Test
    public void disponibilidadParaDiasHabilesFunciona() {
        DisponibilidadHoraria disponibilidadParaDiasHabiles = new DisponibilidadDiasHabiles(LocalTime.of(9, 0),
                LocalTime.of(10, 0));

        assertTrue(disponibilidadParaDiasHabiles.disponible(fixtureHorarios.lunes0901));
        assertFalse(disponibilidadParaDiasHabiles.disponible(fixtureHorarios.lunes1001));
        assertFalse(disponibilidadParaDiasHabiles.disponible(fixtureHorarios.domingo0901));
    }

    @Test
    public void disponibilidadCompuestaFunciona() {
        DisponibilidadHoraria disponibilidadParaLunes =
                new DisponibilidadParaDia(DayOfWeek.MONDAY, LocalTime.of(9, 0), LocalTime.of(10, 0));
        DisponibilidadHoraria disponibilidadParaMiercoles =
                new DisponibilidadParaDia(DayOfWeek.WEDNESDAY, LocalTime.of(9, 0), LocalTime.of(10, 0));
        DisponibilidadHoraria disponibilidadParaLunesYMiercoles =
                new DisponibilidadCompuesta(
                        Arrays.asList(disponibilidadParaLunes, disponibilidadParaMiercoles));

        assertTrue(disponibilidadParaLunesYMiercoles.disponible(fixtureHorarios.lunes0901));
        assertTrue(disponibilidadParaLunesYMiercoles.disponible(fixtureHorarios.miercoles0901));
        assertFalse(disponibilidadParaLunesYMiercoles.disponible(fixtureHorarios.lunes1001));
        assertFalse(disponibilidadParaLunesYMiercoles.disponible(fixtureHorarios.martes0901));
    }
}
