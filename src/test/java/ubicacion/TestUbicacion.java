package ubicacion;

import fixture.FixtureUbicacion;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestUbicacion {
    private static double DELTA_RAZONABLE = 1.0;
    private FixtureUbicacion fixtureUbicacion;

    @Before
    public void setUp() {
        fixtureUbicacion = new FixtureUbicacion();
    }

    @Test
    public void testDistanciaAlMismoPuntoEsCero() {
        assertEquals(0, fixtureUbicacion.ubicacion.distanciaA(fixtureUbicacion.ubicacion), DELTA_RAZONABLE);
    }

    @Test
    public void testDistanciaSeCalculaBien() {
        assertEquals(58, fixtureUbicacion.ubicacion.distanciaA(fixtureUbicacion.ubicacionAMenosDeUnaCuadra), DELTA_RAZONABLE);
        assertEquals(58, fixtureUbicacion.ubicacion.distanciaA(fixtureUbicacion.ubicacionAMenosDe5Cuadras), DELTA_RAZONABLE);
        assertEquals(343, fixtureUbicacion.ubicacion.distanciaA(fixtureUbicacion.ubicacionAMasDeUnaCuadra), DELTA_RAZONABLE);
        assertEquals(1527, fixtureUbicacion.ubicacion.distanciaA(fixtureUbicacion.ubicacionAMasDe5Cuadras), DELTA_RAZONABLE);
        assertEquals(111413, fixtureUbicacion.ubicacion.distanciaA(fixtureUbicacion.ubicacionFueraDeLaComuna), DELTA_RAZONABLE);
    }

    @Test
    public void comunaSabeSiContieneUnaUbicacion() {
        assertTrue(fixtureUbicacion.comuna.contieneA(fixtureUbicacion.ubicacion));
    }

}
