package utils;

/**
 * El flaco te clava el thread actual, es para no escribir tanto trycatch
 */
public class Demorador {

    public Demorador() {
    }

    public void demorar(long unTiempo) {
        try {
            Thread.sleep(unTiempo);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
